-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Янв 12 2021 г., 06:01
-- Версия сервера: 10.4.13-MariaDB-1:10.4.13+maria~bionic
-- Версия PHP: 7.3.20-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_citypuzzle_new`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_available_times`
--

CREATE TABLE `cicada_available_times` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_price_id` int(11) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `disable` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_available_times`
--

INSERT INTO `cicada_available_times` (`id`, `product_price_id`, `time`, `created_at`, `updated_at`, `disable`) VALUES
(1, NULL, '09:59:00', '2020-07-29 02:13:49', '2020-07-29 02:14:00', 0),
(2, 1, '08:00:00', '2020-07-29 02:15:42', '2020-08-05 06:12:40', 0),
(3, 1, '15:11:00', '2020-07-29 23:07:28', '2020-07-29 23:07:28', 0),
(4, 2, '06:05:00', '2020-08-03 04:18:54', '2020-08-05 03:32:37', 0),
(5, 2, '08:50:00', '2020-08-03 04:19:02', '2020-08-05 03:30:28', 0),
(6, 3, '08:06:00', '2020-08-05 05:36:53', '2020-08-05 05:36:53', 0),
(7, 4, '06:09:00', '2020-08-05 05:43:52', '2020-08-05 05:58:52', 1),
(8, 4, '05:20:00', '2020-08-05 06:20:19', '2020-08-05 06:35:55', 1),
(9, 4, '11:59:00', '2020-08-05 06:23:19', '2020-08-05 06:23:19', 0),
(10, 1, '16:00:00', '2020-08-13 11:51:50', '2020-08-13 11:51:50', 0),
(11, 1, '18:00:00', '2020-08-13 11:53:34', '2020-08-13 11:54:12', 0),
(12, 1, '12:20:00', '2020-08-13 12:26:59', '2020-08-13 12:26:59', 0),
(13, 6, '20:58:00', '2020-08-28 11:55:10', '2020-08-28 11:55:10', 0),
(14, 7, '15:00:00', '2020-10-13 06:05:00', '2020-10-13 06:05:00', 0),
(15, 7, '16:00:00', '2020-10-13 06:06:02', '2020-10-13 06:06:02', 0),
(16, 8, '12:00:00', '2020-10-13 06:29:08', '2020-10-13 06:29:08', 0),
(17, 8, '13:00:00', '2020-10-13 06:29:34', '2020-10-13 06:29:34', 0),
(18, 8, '16:00:00', '2020-10-13 06:29:54', '2020-10-13 06:29:54', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_catalogs`
--

CREATE TABLE `cicada_catalogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_cities`
--

CREATE TABLE `cicada_cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sites_name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordinates` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL,
  `visable` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_cities`
--

INSERT INTO `cicada_cities` (`id`, `name`, `name_key`, `sites_name_key`, `coordinates`, `sort`, `visable`, `created_at`, `updated_at`) VALUES
(1, 'Алматы', 'almaty', 'mello_kz', '', 0, 1, '2020-02-02 12:12:09', '2020-02-02 12:12:09');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_column_names`
--

CREATE TABLE `cicada_column_names` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_column_names`
--

INSERT INTO `cicada_column_names` (`id`, `name`, `name_key`, `created_at`, `updated_at`) VALUES
(1, 'Мини описание', 'content_mini', '2020-07-29 03:38:04', '2020-07-29 03:38:04'),
(2, 'Цена', 'price', '2020-07-29 03:38:20', '2020-07-29 03:38:20'),
(3, 'Вопрос', 'Question', '2020-07-30 03:13:19', '2020-07-30 03:13:19');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_dat_times`
--

CREATE TABLE `cicada_dat_times` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `product_pric_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_dat_times`
--

INSERT INTO `cicada_dat_times` (`id`, `date`, `product_pric_id`, `created_at`, `updated_at`) VALUES
(1, '2020-08-13', 5, '2020-08-13 04:58:12', '2020-08-13 04:58:12'),
(2, '2020-08-21', 5, '2020-08-13 04:58:12', '2020-08-13 04:58:12'),
(3, '2020-08-14', 5, '2020-08-13 04:58:12', '2020-08-13 04:58:12'),
(4, '2020-08-15', 5, '2020-08-13 04:58:12', '2020-08-13 04:58:12'),
(5, '2020-08-22', 5, '2020-08-13 04:58:12', '2020-08-13 04:58:12'),
(6, '2020-08-20', 5, '2020-08-13 04:58:12', '2020-08-13 04:58:12'),
(132, '2020-08-28', 6, '2020-08-28 11:55:02', '2020-08-28 11:55:02'),
(133, '2020-08-29', 6, '2020-08-28 11:55:02', '2020-08-28 11:55:02'),
(134, '2020-08-31', 6, '2020-08-28 11:55:02', '2020-08-28 11:55:02'),
(135, '2020-08-30', 6, '2020-08-28 11:55:02', '2020-08-28 11:55:02'),
(136, '2020-08-09', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(137, '2020-08-10', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(138, '2020-08-11', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(139, '2020-08-12', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(140, '2020-08-13', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(141, '2020-08-15', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(142, '2020-08-22', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(143, '2020-08-21', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(144, '2020-08-14', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(145, '2020-08-20', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(146, '2020-08-19', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(147, '2020-08-18', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(148, '2020-08-17', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(149, '2020-08-16', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(150, '2020-09-01', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(151, '2020-09-30', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(152, '2020-09-23', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(153, '2020-09-26', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(154, '2020-09-25', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(155, '2020-09-24', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(156, '2020-09-19', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(157, '2020-09-18', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(158, '2020-09-17', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(159, '2020-09-16', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(160, '2020-09-15', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(161, '2020-09-22', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(162, '2020-09-29', 1, '2020-09-23 09:49:28', '2020-09-23 09:49:28'),
(180, '2020-10-13', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(181, '2020-10-08', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(182, '2020-10-14', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(183, '2020-10-11', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(184, '2020-10-12', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(185, '2020-10-19', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(186, '2020-10-18', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(187, '2020-10-10', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(188, '2020-10-09', 7, '2020-10-13 06:05:25', '2020-10-13 06:05:25'),
(189, '2020-10-13', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(190, '2020-10-07', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(191, '2020-10-14', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(192, '2020-10-15', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(193, '2020-10-08', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(194, '2020-10-09', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(195, '2020-10-16', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(196, '2020-10-17', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(197, '2020-10-10', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(198, '2020-10-06', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(199, '2020-10-05', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(200, '2020-10-04', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(201, '2020-10-11', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(202, '2020-10-12', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(203, '2020-10-19', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59'),
(204, '2020-10-18', 8, '2020-10-13 06:28:59', '2020-10-13 06:28:59');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_failed_jobs`
--

CREATE TABLE `cicada_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_faqs`
--

CREATE TABLE `cicada_faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_faqs`
--

INSERT INTO `cicada_faqs` (`id`, `sort`, `question`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, '{\"ru\":\"Какая цель игры?\",\"en\":\"фывфы\",\"fr\":\"фывфы\",\"kz\":\"Вопрос 1\"}', '{\"ru\":\"<p>Необходимо пройти все задания и дойти до концовки сюжета.<\\/p>\",\"en\":\"<p>ыфвыф<\\/p>\",\"fr\":\"<p>ыфвыф<\\/p>\",\"kz\":\"<p>ответ 1<\\/p>\"}', '2020-07-30 03:11:04', '2020-09-24 06:03:04'),
(2, 2, '{\"ru\":\"Могу ли я в одиночку пройти игру?\",\"en\":\"Вопрос 2\",\"fr\":\"Вопрос 2\",\"kz\":\"Вопрос 2\"}', '{\"ru\":\"<p>Можете, но есть шанс что вы застрянете на одной из загадок и потеряете время, ведь две головы лучше чем одна.<\\/p>\",\"en\":\"<p>Ответ 2<\\/p>\",\"fr\":\"<p>Ответ 2<\\/p>\",\"kz\":\"<p>Ответ 2<\\/p>\"}', '2020-07-30 03:15:41', '2020-09-24 06:09:59'),
(3, 3, '{\"ru\":\"Нужны ли мне знания математики физики и химии?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p>Нет мы не на столько жестокие чтобы возвращать вас за школьную скамью.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:04:14', '2020-09-24 06:09:59'),
(4, 4, '{\"ru\":\"Могу ли я использовать поиск Google для помощи?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p>Конечно можете, если это вам поможет, но это мало вероятно.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:04:54', '2020-09-24 06:09:59'),
(5, 5, '{\"ru\":\"Могу ли я использовать свои сверх способности?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p>Мы не советуем этого делать так как перед остальными обычными смертными это не честно.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:06:22', '2020-09-24 06:09:59'),
(6, 6, '{\"ru\":\"Если мы не сможем пройти игру, что будет?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p>Будет жалко, но мы будем молится за вас.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:07:23', '2020-09-24 06:09:59'),
(7, 7, '{\"ru\":\"Нужно ли мне применять силу во время игры?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm;\\\">Самая большая нагрузка будет на вашу логику и может быть ноги.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:07:46', '2020-09-24 06:09:59'),
(8, 8, '{\"ru\":\"Во что я должен\\/должна быть одет\\/а?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm;\\\">Будьте в том в чем вам комфортно.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:08:08', '2020-09-24 06:09:59'),
(9, 9, '{\"ru\":\"Если пойдёт дождь, нужно ли мне продолжать игру?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm;\\\">Конечно нужно, если только вы не из бумаги.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:08:31', '2020-09-24 06:09:59'),
(10, 10, '{\"ru\":\"Какие языки я должен\\/должна знать?\",\"en\":null,\"fr\":null,\"kz\":null}', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm;\\\">Наша игра может проходить на Французском, Английском или же Русском языках.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', '2020-09-24 06:08:53', '2020-09-24 06:09:59');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_filters`
--

CREATE TABLE `cicada_filters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_filter_attitudes`
--

CREATE TABLE `cicada_filter_attitudes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `FilterItem_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_filter_items`
--

CREATE TABLE `cicada_filter_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Filter_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_languages`
--

CREATE TABLE `cicada_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visable` int(11) NOT NULL DEFAULT 0,
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_languages`
--

INSERT INTO `cicada_languages` (`id`, `name`, `name_key`, `visable`, `alternative`, `created_at`, `updated_at`) VALUES
(1, 'RU', 'ru', 1, '', NULL, '2020-07-29 06:00:24'),
(2, 'EN', 'en', 1, NULL, NULL, '2020-07-29 06:00:16'),
(3, 'FR', 'fr', 0, NULL, '2020-07-29 05:38:29', '2020-07-29 06:00:08');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_logs`
--

CREATE TABLE `cicada_logs` (
  `log` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_migrations`
--

CREATE TABLE `cicada_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_migrations`
--

INSERT INTO `cicada_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_19_080231_create_permission_grups_table', 1),
(4, '2017_10_19_080327_create_permission_users_table', 1),
(5, '2018_02_06_074012_create_permission_dates_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2019_09_05_180425_create_languages_table', 1),
(8, '2019_09_05_203111_create_cities_table', 1),
(9, '2019_09_05_203252_create_orders_table', 1),
(10, '2019_09_05_203408_create_filters_table', 1),
(11, '2019_09_05_203421_create_filter_items_table', 1),
(12, '2019_09_05_203457_create_order_items_table', 1),
(13, '2019_09_05_204200_create_filter_attitudes_table', 1),
(14, '2019_09_05_204815_create_products_table', 1),
(15, '2019_09_05_204905_create_product_prices_table', 1),
(16, '2019_09_05_205437_create_catalogs_table', 1),
(17, '2019_09_05_214923_create_model_lists_table', 1),
(18, '2019_09_05_221710_create_model_metas_table', 1),
(19, '2019_09_05_222221_create_nav_menus_table', 1),
(20, '2019_09_05_224414_create_column_names_table', 1),
(21, '2019_12_20_050016_create_sites_table', 1),
(22, '2019_12_20_050043_create_shops_table', 1),
(23, '2019_12_26_131411_create_stocks_table', 1),
(24, '2019_12_26_131458_create_questions_table', 1),
(25, '2019_12_27_042414_create_news_items_table', 1),
(26, '2020_07_29_075242_create_available_times_table', 2),
(27, '2020_07_30_062941_create_static_texts_table', 3),
(28, '2020_07_30_090825_create_faqs_table', 4),
(33, '2019_05_03_000001_create_customer_columns', 5),
(34, '2019_05_03_000002_create_subscriptions_table', 5),
(35, '2019_05_03_000003_create_subscription_items_table', 5),
(36, '2020_08_13_104440_create_dat_times_table', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_model_lists`
--

CREATE TABLE `cicada_model_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_model_lists`
--

INSERT INTO `cicada_model_lists` (`id`, `name`, `name_key`, `created_at`, `updated_at`, `icon`, `sort`) VALUES
(1, 'Каталог', 'Catalog', NULL, NULL, NULL, 0),
(2, 'Игры', 'Product', NULL, '2020-07-29 00:50:12', '<span class=\"oi\" data-glyph=\"puzzle-piece\"></span>', 0),
(3, 'Доступное время', 'Product_price', NULL, '2020-07-29 01:41:06', NULL, 0),
(4, 'Названия Фильтров', 'Filter', NULL, NULL, NULL, 0),
(5, 'Пункты Фильтров', 'Filter_item', NULL, NULL, NULL, 0),
(6, 'Связи Фильтров', 'Filter_attitudes', NULL, NULL, NULL, 0),
(7, 'Заказы', 'Order', NULL, '2020-06-21 04:28:06', '<span class=\"oi\" data-glyph=\"script\"></span>', 0),
(8, 'Информация о заказе', 'Order_item', NULL, NULL, NULL, 0),
(9, 'Пользователи', 'User', NULL, NULL, NULL, 0),
(10, 'Сайты', 'Site', NULL, NULL, NULL, 0),
(11, 'Города', 'City', NULL, NULL, NULL, 0),
(12, 'Магазины', 'Shop', NULL, NULL, NULL, 0),
(13, 'Языки', 'Language', NULL, '2020-07-29 05:59:51', '<span class=\"oi\" data-glyph=\"comment-square\"></span>', 0),
(14, 'Время', 'AvailableTime', NULL, NULL, NULL, 0),
(15, 'Текста', 'StaticText', NULL, '2020-07-30 00:34:37', '<span class=\"oi\" data-glyph=\"rain\"></span>', 0),
(16, 'FAQ', 'Faq', NULL, '2020-07-30 03:10:40', '<span class=\"oi\" data-glyph=\"clipboard\"></span>', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_model_metas`
--

CREATE TABLE `cicada_model_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `languages` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_model_metas`
--

INSERT INTO `cicada_model_metas` (`id`, `name`, `name_key`, `sort`, `type`, `attachment`, `created_at`, `updated_at`, `languages`) VALUES
(113, NULL, 'FilterItem_id', '0', 'table_catalog', 'Filter_attitudes', '2020-06-21 04:26:15', '2020-06-21 04:26:15', 0),
(114, NULL, 'model_id', '0', 'table_catalog', 'Filter_attitudes', '2020-06-21 04:26:15', '2020-06-21 04:26:15', 0),
(115, NULL, 'FilterItem_id', '0', 'table_save', 'Filter_attitudes', '2020-06-21 04:26:15', '2020-06-21 04:26:15', 0),
(221, NULL, 'id', '0', 'table_catalog', 'Catalog', '2020-07-28 02:56:44', '2020-07-28 02:56:44', 0),
(222, NULL, 'title', '0', 'table_catalog', 'Catalog', '2020-07-28 02:56:44', '2020-07-28 02:56:44', 0),
(223, NULL, 'title', '0', 'table_save', 'Catalog', '2020-07-28 02:56:44', '2020-07-28 02:56:44', 0),
(224, NULL, 'slug', '0', 'table_save', 'Catalog', '2020-07-28 02:56:44', '2020-07-28 02:56:44', 0),
(281, NULL, 'id', '0', 'table_catalog', 'AvailableTime', '2020-07-29 01:56:13', '2020-07-29 01:56:13', 0),
(282, NULL, 'time', '0', 'table_catalog', 'AvailableTime', '2020-07-29 01:56:13', '2020-07-29 01:56:13', 0),
(283, NULL, 'time', '0', 'table_save', 'AvailableTime', '2020-07-29 01:56:13', '2020-07-29 01:56:13', 0),
(429, NULL, 'id', '0', 'table_catalog', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(430, NULL, 'title', '0', 'table_catalog', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(431, NULL, 'images', '0', 'table_save', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(432, NULL, 'title', '0', 'table_save', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 1),
(433, NULL, 'content', '0', 'table_save', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 1),
(434, NULL, 'price', '0', 'table_save', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(435, NULL, 'content_mini', '0', 'table_save', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(436, NULL, 'video', '0', 'table_save', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(437, NULL, '1', '0', 'table_catalog_availability', 'Product', '2020-07-29 05:37:23', '2020-07-29 05:37:23', 0),
(438, NULL, 'name', '0', 'table_catalog', 'Language', '2020-07-29 05:59:51', '2020-07-29 05:59:51', 0),
(439, NULL, 'name_key', '0', 'table_catalog', 'Language', '2020-07-29 05:59:51', '2020-07-29 05:59:51', 0),
(440, NULL, 'name', '0', 'table_save', 'Language', '2020-07-29 05:59:51', '2020-07-29 05:59:51', 0),
(441, NULL, 'name_key', '0', 'table_save', 'Language', '2020-07-29 05:59:51', '2020-07-29 05:59:51', 0),
(442, NULL, '1', '0', 'table_catalog_availability', 'Language', '2020-07-29 05:59:51', '2020-07-29 05:59:51', 0),
(455, NULL, 'name_key', '0', 'table_catalog', 'StaticText', '2020-07-30 01:40:15', '2020-07-30 01:40:15', 0),
(456, NULL, 'name_key', '0', 'table_save', 'StaticText', '2020-07-30 01:40:15', '2020-07-30 01:40:15', 0),
(457, NULL, 'content', '0', 'table_save', 'StaticText', '2020-07-30 01:40:15', '2020-07-30 01:40:15', 1),
(467, NULL, 'id', '0', 'table_catalog', 'Faq', '2020-07-30 03:16:48', '2020-07-30 03:16:48', 0),
(468, NULL, 'question', '0', 'table_catalog', 'Faq', '2020-07-30 03:16:48', '2020-07-30 03:16:48', 0),
(469, NULL, 'question', '0', 'table_save', 'Faq', '2020-07-30 03:16:48', '2020-07-30 03:16:48', 1),
(470, NULL, 'content', '0', 'table_save', 'Faq', '2020-07-30 03:16:48', '2020-07-30 03:16:48', 1),
(471, NULL, '1', '0', 'table_catalog_availability', 'Faq', '2020-07-30 03:16:48', '2020-07-30 03:16:48', 0),
(495, NULL, 'team_name', '0', 'table_catalog', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(496, NULL, 'team_people', '0', 'table_catalog', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(497, NULL, 'first_name', '0', 'table_catalog', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(498, NULL, 'last_name', '0', 'table_catalog', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(499, NULL, 'mobile_number', '0', 'table_catalog', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(500, NULL, 'email', '0', 'table_catalog', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(501, NULL, 'team_name', '0', 'table_save', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(502, NULL, 'team_people', '0', 'table_save', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(503, NULL, 'first_name', '0', 'table_save', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(504, NULL, 'last_name', '0', 'table_save', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(505, NULL, 'mobile_number', '0', 'table_save', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(506, NULL, 'email', '0', 'table_save', 'Order_item', '2020-08-05 01:12:36', '2020-08-05 01:12:36', 0),
(516, NULL, 'id', '0', 'table_catalog', 'Order', '2020-08-05 01:30:07', '2020-08-05 01:30:07', 0),
(517, NULL, 'price', '0', 'table_catalog', 'Order', '2020-08-05 01:30:07', '2020-08-05 01:30:07', 0),
(518, NULL, 'order_number', '0', 'table_catalog', 'Order', '2020-08-05 01:30:07', '2020-08-05 01:30:07', 0),
(519, NULL, 'status', '0', 'table_catalog', 'Order', '2020-08-05 01:30:07', '2020-08-05 01:30:07', 0),
(520, NULL, 'created_at', '0', 'table_catalog', 'Order', '2020-08-05 01:30:07', '2020-08-05 01:30:07', 0),
(521, NULL, '1', '0', 'table_catalog_availability', 'Order', '2020-08-05 01:30:07', '2020-08-05 01:30:07', 0),
(522, NULL, 'id', '0', 'table_catalog', 'Product_price', '2020-08-13 05:01:12', '2020-08-13 05:01:12', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_nav_menus`
--

CREATE TABLE `cicada_nav_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_db` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_news_items`
--

CREATE TABLE `cicada_news_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_orders`
--

CREATE TABLE `cicada_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `User_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_old` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promocode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `teamInfo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `time_id` int(11) DEFAULT NULL,
  `bay_online` int(11) NOT NULL DEFAULT 0,
  `session_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_orders`
--

INSERT INTO `cicada_orders` (`id`, `User_id`, `name`, `email`, `phone`, `price_old`, `price`, `payment`, `order_number`, `status`, `delivery`, `delivery_price`, `promocode`, `comment`, `address`, `created_at`, `updated_at`, `teamInfo`, `date`, `time`, `game_id`, `time_id`, `bay_online`, `session_id`) VALUES
(21, NULL, NULL, NULL, NULL, NULL, 70, NULL, '865930', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-05 03:30:28', '2020-08-05 03:30:28', '[{\"team_people\":\"2\",\"team_name\":\"asd\",\"first_name\":\"d\",\"last_name\":\"sd\",\"mobile_number\":\"d\",\"email\":\"sdada@sd.ru\"}]', '2020-08-15', '08:50', 1, 5, 0, NULL),
(22, NULL, NULL, NULL, NULL, NULL, 70, NULL, '502516', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-05 03:32:37', '2020-08-05 03:32:37', '[{\"team_people\":\"2\",\"team_name\":\"asd\",\"first_name\":\"sd\",\"last_name\":\"sd\",\"mobile_number\":\"s\",\"email\":\"asd@asd.ru\"}]', '2020-08-15', '06:05', 1, 4, 0, NULL),
(23, NULL, NULL, NULL, NULL, NULL, 42, NULL, '998886', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-05 05:58:52', '2020-08-05 05:58:52', '[{\"team_people\":\"2\",\"team_name\":\"asd\",\"first_name\":\"as\",\"last_name\":\"asdasdd\",\"mobile_number\":\"d\",\"email\":\"sd@asd.ru\"}]', '2020-08-06', '06:09', 2, 7, 0, NULL),
(24, NULL, NULL, NULL, NULL, NULL, 42, NULL, '174907', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-05 06:35:55', '2020-08-05 06:35:55', '[{\"team_people\":\"2\",\"team_name\":\"we\",\"first_name\":\"we\",\"last_name\":\"w\",\"mobile_number\":\"r\",\"email\":\"rwer@sadasd.ru\"}]', '2020-08-06', '05:20', 2, 8, 0, NULL),
(25, NULL, NULL, NULL, NULL, NULL, 70, NULL, '738862', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-13 11:58:16', '2020-08-13 11:58:17', '[{\"team_people\":\"2\",\"team_name\":\"asd\",\"first_name\":\"d\",\"last_name\":\"dasdaasas\",\"mobile_number\":\"asda\",\"email\":\"dasdasD@asd.ru\"}]', '2020-08-11', '18:00', 1, 0, 1, 'cs_test_v0gc0d3CXDilpo6BnYNxrxdcQFpMvQCKzoOf9pv2Z0UZR3M4aIE5xH7b'),
(26, NULL, NULL, NULL, NULL, NULL, 70, NULL, '1071', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-13 12:21:50', '2020-08-13 12:21:51', '[{\"team_people\":\"2\",\"team_name\":\"as\",\"first_name\":\"d\",\"last_name\":\"asdasdasdasd\",\"mobile_number\":\"asdasd\",\"email\":\"asdas@asd.ru\"}]', '2020-08-09', '08:00', 1, 0, 0, 'cs_test_dNuB88Rb9haD13oX7UqLPxEwoLwhB7lV2JhkcZzBOH77tXMMrgjTtDEB'),
(27, NULL, NULL, NULL, NULL, NULL, 70, NULL, '377300', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-13 12:22:48', '2020-08-13 12:23:30', '[{\"team_people\":\"2\",\"team_name\":\"asd\",\"first_name\":\"as\",\"last_name\":\"asdasdddasd\",\"mobile_number\":\"sadasd\",\"email\":\"asdSA@sd.ru\"}]', '2020-08-09', '15:11', 1, 0, 1, 'cs_test_fK6UAkQrO0Tz6ReB3Wn6PzsXp2Ib3L9V9ebSwDeSGh15bpAAzYr7ZN8n'),
(28, NULL, NULL, NULL, NULL, NULL, 70, NULL, '438409', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-13 12:26:06', '2020-08-13 12:26:07', '[{\"team_people\":\"2\",\"team_name\":\"фыв\",\"first_name\":\"фы\",\"last_name\":\"в\",\"mobile_number\":\"в\",\"email\":\"asdasd@asd.ru\"}]', '2020-08-09', '16:00', 1, 0, 0, 'cs_test_Cyy1346GzxLm3scGOhMFbYFxhBwEBJeqaj3baDzONw3XWmZWgOi4rjoT'),
(29, NULL, NULL, NULL, NULL, NULL, 70, NULL, '440002', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-13 12:26:21', '2020-08-13 12:26:22', '[{\"team_people\":\"2\",\"team_name\":\"asd\",\"first_name\":\"d\",\"last_name\":\"d\",\"mobile_number\":\"sdasdasd\",\"email\":\"asdsaD@asd.ru\"}]', '2020-08-09', '18:00', 1, 0, 0, 'cs_test_U4w1IY3xrc2Y8kMhwontPfKowuX3m0ny6AECROmQDgWDkOvE7Rk265Dw'),
(30, NULL, NULL, NULL, NULL, NULL, 350, NULL, '786446', 'new', NULL, NULL, NULL, NULL, NULL, '2020-08-14 13:10:18', '2020-08-14 13:12:08', '[{\"team_people\":\"5\",\"team_name\":\"Testing\",\"first_name\":\"Mr Felix\",\"last_name\":\"Moncler \",\"mobile_number\":\"0763888767\",\"email\":\"beka777777@mail.ru\"},{\"team_people\":\"5\",\"team_name\":\"Test 2\",\"first_name\":\"Mario\",\"last_name\":\"Pizza\",\"mobile_number\":\"073222333\",\"email\":\"info@virtual-sphere.ch\"}]', '2020-08-11', '12:20', 1, 0, 1, 'cs_test_pprJwWAuzOTYaIrZpS2RPOC5jY1tFPW0EHSXFKuJZfuuW1ATfzdcx1Bs'),
(31, NULL, NULL, NULL, NULL, NULL, 70, NULL, '616810', 'new', NULL, NULL, NULL, NULL, NULL, '2020-10-08 12:26:11', '2020-10-08 12:26:12', '[{\"team_people\":\"2\",\"team_name\":\"укпвкап\",\"first_name\":\"впвап\",\"last_name\":\"вапва\",\"mobile_number\":\"вап\",\"email\":\"reception.kz@loreal.com\"}]', '2020-09-16', '15:11', 1, 0, 0, 'cs_test_PApYLeMkqZ2iyXKqGJqz4Xn2YMA6Qf0MuJLEok0xNlrGoQFkAESLd0Un'),
(32, NULL, NULL, NULL, NULL, NULL, 444, NULL, '995776', 'new', NULL, NULL, NULL, NULL, NULL, '2020-11-06 11:45:08', '2020-11-06 11:45:45', '[{\"team_people\":\"2\",\"team_name\":\"Oljas\",\"first_name\":\"Oljas\",\"last_name\":\"Oljas\",\"mobile_number\":\"+77089681134\",\"email\":\"test@mail.ru\"}]', '2020-10-08', '16:00', 3, 0, 1, 'cs_test_OZIsDIdZMaHrtJYYjOw4E2e4YR6xsWJAd3LgJef0yZnaSmatrNzSqYG0');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_order_items`
--

CREATE TABLE `cicada_order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_people` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orders_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_order_items`
--

INSERT INTO `cicada_order_items` (`id`, `team_name`, `team_people`, `first_name`, `last_name`, `mobile_number`, `email`, `status`, `orders_id`, `created_at`, `updated_at`) VALUES
(16, 'asd', '2', 'd', 'sd', 'd', 'sdada@sd.ru', NULL, 21, '2020-08-05 03:30:28', '2020-08-05 03:30:28'),
(17, 'asd', '2', 'sd', 'sd', 's', 'asd@asd.ru', NULL, 22, '2020-08-05 03:32:37', '2020-08-05 03:32:37'),
(18, 'asd', '2', 'as', 'asdasdd', 'd', 'sd@asd.ru', NULL, 23, '2020-08-05 05:58:52', '2020-08-05 05:58:52'),
(19, 'we', '2', 'we', 'w', 'r', 'rwer@sadasd.ru', NULL, 24, '2020-08-05 06:35:55', '2020-08-05 06:35:55'),
(20, 'asd', '2', 'd', 'dasdaasas', 'asda', 'dasdasD@asd.ru', NULL, 25, '2020-08-13 11:58:16', '2020-08-13 11:58:16'),
(21, 'as', '2', 'd', 'asdasdasdasd', 'asdasd', 'asdas@asd.ru', NULL, 26, '2020-08-13 12:21:50', '2020-08-13 12:21:50'),
(22, 'asd', '2', 'as', 'asdasdddasd', 'sadasd', 'asdSA@sd.ru', NULL, 27, '2020-08-13 12:22:48', '2020-08-13 12:22:48'),
(23, 'фыв', '2', 'фы', 'в', 'в', 'asdasd@asd.ru', NULL, 28, '2020-08-13 12:26:06', '2020-08-13 12:26:06'),
(24, 'asd', '2', 'd', 'd', 'sdasdasd', 'asdsaD@asd.ru', NULL, 29, '2020-08-13 12:26:21', '2020-08-13 12:26:21'),
(25, 'Testing', '5', 'Mr Felix', 'Moncler ', '0763888767', 'beka777777@mail.ru', NULL, 30, '2020-08-14 13:10:18', '2020-08-14 13:10:18'),
(26, 'Test 2', '5', 'Mario', 'Pizza', '073222333', 'info@virtual-sphere.ch', NULL, 30, '2020-08-14 13:10:18', '2020-08-14 13:10:18'),
(27, 'укпвкап', '2', 'впвап', 'вапва', 'вап', 'reception.kz@loreal.com', NULL, 31, '2020-10-08 12:26:11', '2020-10-08 12:26:11'),
(28, 'Oljas', '2', 'Oljas', 'Oljas', '+77089681134', 'test@mail.ru', NULL, 32, '2020-11-06 11:45:08', '2020-11-06 11:45:08');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_password_resets`
--

CREATE TABLE `cicada_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_permission_dates`
--

CREATE TABLE `cicada_permission_dates` (
  `id` int(10) UNSIGNED NOT NULL,
  `permision_group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `crud` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'select',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'db',
  `assess` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_permission_dates`
--

INSERT INTO `cicada_permission_dates` (`id`, `permision_group`, `table`, `crud`, `type`, `assess`, `created_at`, `updated_at`) VALUES
(1, '1', 'all', 'select', 'db', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_permission_grups`
--

CREATE TABLE `cicada_permission_grups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_permission_grups`
--

INSERT INTO `cicada_permission_grups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL),
(2, 'manage', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_permission_users`
--

CREATE TABLE `cicada_permission_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `grup_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_permission_users`
--

INSERT INTO `cicada_permission_users` (`id`, `user_id`, `grup_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_products`
--

CREATE TABLE `cicada_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ed_massa` int(11) NOT NULL DEFAULT 0,
  `ed_massa2` int(11) NOT NULL DEFAULT 0,
  `ed_count` int(11) NOT NULL DEFAULT 0,
  `let` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content_mini` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_products`
--

INSERT INTO `cicada_products` (`id`, `images`, `title`, `content`, `ed_massa`, `ed_massa2`, `ed_count`, `let`, `tags`, `price`, `size`, `slug`, `path`, `created_at`, `updated_at`, `content_mini`, `video`) VALUES
(1, '/public/media/Update/image_png/BZBQ2_file.png', '{\"ru\":\"<p>HUNT FOR THE CHESTER CAT 2<\\/p>\",\"en\":\"<p>HUNT FOR THE CHESTER CAT<\\/p>\",\"fr\":\"<p>HUNT FOR THE CHESTER CAT<\\/p>\"}', '{\"ru\":\"<p>Ru text<\\/p>\",\"en\":\"<p>en text<\\/p>\",\"fr\":\"<p>fr text<\\/p>\"}', 0, 0, 0, 0, NULL, '35', NULL, 'hunt-for-the-chester-cat-2', 'hunt-for-the-chester-cat-2', '2020-06-21 08:32:57', '2020-08-05 02:13:45', 'The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/IfxZV9v0KNQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(2, '/public/media/Update/image_jpeg/Ul2GB_file.jpg', '{\"ru\":\"<p>GAME 1<\\/p>\",\"en\":\"<p>game 2<\\/p>\",\"fr\":\"<p>game 2<\\/p>\",\"kz\":\"<p>game 2<\\/p>\"}', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm; tab-stops: 28.0pt 56.0pt 84.0pt 112.0pt 140.0pt 168.0pt 196.0pt 224.0pt 252.0pt 280.0pt 308.0pt 336.0pt 364.0pt 392.0pt 420.0pt 448.0pt 476.0pt;\\\">Добро пожаловать в 1816 год, теперь вам предстоит окунуться в роль лорда Байрона и его друзей Мэри Годвин и Перси Шелли. Если вы не знаете эту историю, то советуем вам прочувствовать всё это на себе, так как именно на территории Женевы началась история Франкенштейна.<\\/p>\",\"en\":\"<p>asdas<\\/p>\",\"fr\":\"<p>asdas<\\/p>\",\"kz\":\"<p>asdas<\\/p>\"}', 0, 0, 0, 0, NULL, '15', NULL, 'game-1-2', 'game-1-2', '2020-08-05 05:33:26', '2020-09-24 06:01:04', NULL, NULL),
(3, 'аспми', '{\"ru\":\"<p>опмритьб<\\/p>\",\"en\":\"<p>опмритьб<\\/p>\",\"fr\":\"<p>опмритьб<\\/p>\"}', '{\"ru\":\"<p>па ро<\\/p>\",\"en\":\"<p>па ро<\\/p>\",\"fr\":\"<p>па ро<\\/p>\"}', 0, 0, 0, 0, NULL, '222', NULL, 'opmritb-3', 'opmritb-3', '2020-10-13 06:04:06', '2020-10-13 06:05:49', 'ап р', NULL),
(4, NULL, '{\"ru\":\"<p>вкаепнр<\\/p>\",\"en\":\"<p>вкаепнр<\\/p>\",\"fr\":\"<p>вкаепнр<\\/p>\"}', '{\"ru\":\"<p>сапмирто<\\/p>\",\"en\":\"<p>сапмирто<\\/p>\",\"fr\":\"<p>сапмирто<\\/p>\"}', 0, 0, 0, 0, NULL, '222', NULL, 'vkaepnr-4', 'vkaepnr-4', '2020-10-13 06:28:43', '2020-10-13 06:29:19', 'спмри', 'па');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_product_prices`
--

CREATE TABLE `cicada_product_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Product_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_product_prices`
--

INSERT INTO `cicada_product_prices` (`id`, `created_at`, `updated_at`, `Product_id`) VALUES
(1, '2020-06-21 08:45:16', '2020-08-05 06:00:27', 1),
(5, '2020-08-13 04:51:26', '2020-08-13 04:51:26', 1),
(6, '2020-08-28 11:55:02', '2020-08-28 11:55:02', 2),
(7, '2020-10-13 06:04:38', '2020-10-13 06:04:38', 3),
(8, '2020-10-13 06:28:59', '2020-10-13 06:28:59', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_questions`
--

CREATE TABLE `cicada_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalog_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_shops`
--

CREATE TABLE `cicada_shops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `graphic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coordinates` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cities_name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_sites`
--

CREATE TABLE `cicada_sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_sites`
--

INSERT INTO `cicada_sites` (`id`, `name`, `name_key`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'mello.thefactory.kz', 'mello_kz', '0', '2020-02-02 12:12:09', '2020-02-02 12:12:09');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_static_texts`
--

CREATE TABLE `cicada_static_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type_input` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `languages` int(11) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_static_texts`
--

INSERT INTO `cicada_static_texts` (`id`, `name_key`, `content`, `page`, `created_at`, `updated_at`, `type_input`, `languages`, `sort`) VALUES
(6, 'Slider compani name', '{\"ru\":\"city puzzle\",\"en\":\"city puzzle\",\"fr\":\"city puzzle\",\"kz\":\"city puzzle\"}', 'Главная', '2020-07-30 02:08:54', '2020-09-24 05:53:59', 'text', 1, 0),
(7, 'Slider title', '{\"ru\":\"<p>Окунитесь в первый познавательный квест в Женеве.<\\/p>\",\"en\":\"<p>city puzzle 231 asd&nbsp;<\\/p>\",\"fr\":\"<p>city puzzle asd&nbsp;<\\/p>\",\"kz\":\"<p>КВЕСТ НА ОТКРЫТОМ ВОЗДУХЕ 22 2<\\/p>\"}', 'Главная', '2020-07-30 02:08:54', '2020-09-24 05:53:59', 'textarea', 1, 0),
(8, 'Slider right text', '{\"ru\":\"Когда еще вы сможете насладится настоящей историей этого города как не сегодня с нами.\",\"en\":\"Lorem ipsum dolor sit amet\",\"fr\":\"Lorem ipsum dolor sit amet\",\"kz\":\"Lorem ipsum dolor sit amet\"}', 'Главная', '2020-07-30 02:08:54', '2020-09-24 05:59:30', 'text', 1, 0),
(9, 'Slider right text 2', '{\"ru\":\"Проходя каждую нашу игру, если ее конечно можно так назвать вы станете настоящим знатоком, своего города и вы всегда сможете блеснуть частичкой знаний, которые вы получите если сможете пройти наш квест от начала до конца.\",\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.\",\"fr\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.\",\"kz\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.\"}', 'Главная', '2020-07-30 02:08:54', '2020-09-24 05:54:34', 'text', 1, 0),
(10, 'button down', '{\"ru\":\"листай вниз\",\"en\":\"листай вниз\",\"fr\":\"листай вниз\",\"kz\":\"листай вниз\"}', 'Главная', '2020-07-30 02:21:31', '2020-09-24 05:53:59', 'text', 1, 0),
(11, 'Video button', '{\"ru\":\"Смотреть видео\",\"en\":\"Смотреть видео\",\"fr\":\"Смотреть видео\",\"kz\":\"Смотреть видео\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'text', 1, 0),
(12, 'Video title', '{\"ru\":\"<p>О компании<\\/p>\",\"en\":\"<p>О компании<\\/p>\",\"fr\":\"<p>О компании<\\/p>\",\"kz\":\"<p>О компании<\\/p>\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'textarea', 1, 0),
(13, 'Video content', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm; tab-stops: 28.0pt 56.0pt 84.0pt 112.0pt 140.0pt 168.0pt 196.0pt 224.0pt 252.0pt 280.0pt 308.0pt 336.0pt 364.0pt 392.0pt 420.0pt 448.0pt 476.0pt;\\\">Мы предлагаем вам различные исторические и легендарно мистические сценарии основаны на реальных историях и легендах города Женевы. Благодаря долгим и скрупулёзным исследованиям с ведущими писателями и сценаристами наша команда разработала максимально живой и интересный вид Эскейп Рума.<\\/p>\",\"en\":null,\"fr\":null,\"kz\":null}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:59:30', 'textarea', 1, 0),
(14, 'Recommendation before', '{\"ru\":\"это вам точно понравится\",\"en\":\"это вам точно понравится\",\"fr\":\"это вам точно понравится\",\"kz\":\"это вам точно понравится\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'text', 1, 0),
(15, 'Recommendation title', '{\"ru\":\"<p>наши лучшие игры<\\/p>\",\"en\":\"<p>наши лучшие игры<\\/p>\",\"fr\":\"<p>наши лучшие игры<\\/p>\",\"kz\":\"<p>наши лучшие игры<\\/p>\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'textarea', 1, 0),
(16, 'Game №', '{\"ru\":\"Игра номер\",\"en\":\"Игра номер\",\"fr\":\"Игра номер\",\"kz\":\"Игра номер\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'text', 1, 0),
(17, 'button reservation', '{\"ru\":\"Забронировать\",\"en\":\"Забронировать\",\"fr\":\"Забронировать\",\"kz\":\"Забронировать\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'text', 1, 0),
(18, 'Event after', '{\"ru\":\"здесь вы узнаете\",\"en\":\"здесь вы узнаете\",\"fr\":\"здесь вы узнаете\",\"kz\":\"здесь вы узнаете\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'text', 1, 0),
(19, 'Event title', '{\"ru\":\"<p>наши мероприятия<\\/p>\",\"en\":\"<p>наши мероприятия<\\/p>\",\"fr\":\"<p>наши мероприятия<\\/p>\",\"kz\":\"<p>наши мероприятия<\\/p>\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'textarea', 1, 0),
(20, 'Event content', '{\"ru\":\"<p class=\\\"Default\\\" style=\\\"margin-top: 0cm; tab-stops: 28.0pt 56.0pt 84.0pt 112.0pt 140.0pt 168.0pt 196.0pt 224.0pt 252.0pt 280.0pt 308.0pt 336.0pt 364.0pt 392.0pt 420.0pt 448.0pt 476.0pt;\\\">Зачем платить кому-то чтобы вас заперли в комнате с необходимостью найти выход когда можно совместить прогулку по городу с увлекательной игрой для разгадки таинственных обрядов и деяний. Вместе с нами вы можете отметить ваш День Рождения или Корпоратив, как никогда раньше и сделать его по-настоящему незабываемым для коллег и друзей. В добавок ко всему этому мы можем лично для вас изменить сценарий игры и превратить Вас в его главного героя или же жертву. Но знайте мы не такие добрые как кажемся, поэтому мы приготовили для вас неожиданные сюрпризы и будем считать каждую вашу секунду потраченную впустую, так как у нас в Женеве &ldquo;Время пишет историю&rdquo;.<\\/p>\\r\\n<p class=\\\"Default\\\" style=\\\"margin-top: 0cm; tab-stops: 28.0pt 56.0pt 84.0pt 112.0pt 140.0pt 168.0pt 196.0pt 224.0pt 252.0pt 280.0pt 308.0pt 336.0pt 364.0pt 392.0pt 420.0pt 448.0pt 476.0pt;\\\">&nbsp;<\\/p>\",\"en\":\"<p>The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd.<\\/p>\",\"fr\":\"<p>The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd.<\\/p>\",\"kz\":\"<p>The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd.<\\/p>\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:56:41', 'textarea', 1, 0),
(21, 'Event button', '{\"ru\":\"Смотреть все мероприятия\",\"en\":\"<p>Смотреть все мероприятия<\\/p>\",\"fr\":\"<p>Смотреть все мероприятия<\\/p>\",\"kz\":\"Смотреть все мероприятия\"}', 'Главная', '2020-07-30 02:27:47', '2020-09-24 05:53:59', 'text', 1, 0),
(22, 'FAQ title', '{\"ru\":\"<p>Вопрос\\/ответ<\\/p>\",\"en\":\"<p>ответы и вопросы<\\/p>\",\"fr\":\"<p>ответы и вопросы<\\/p>\",\"kz\":\"<p>ответы и вопросы<\\/p>\"}', 'FAQ', '2020-07-30 03:19:53', '2020-09-24 06:12:59', 'textarea', 1, 0),
(23, 'FAQ after', '{\"ru\":\"Если вы не нашли ответа на свой вопрос, пожалуйста напишите нам на почту, мы обязательно ответим.\",\"en\":\"text\",\"fr\":\"text\",\"kz\":\"text\"}', 'FAQ', '2020-07-30 03:19:53', '2020-09-24 06:15:41', 'text', 1, 0),
(24, 'ACTIVITY before', '{\"ru\":\"The game\",\"en\":\"The game\",\"fr\":\"The game\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:55', 'text', 1, 0),
(25, 'ACTIVITY title', '{\"ru\":\"<p>НАШИ МЕРОПРИЯТИЯ<\\/p>\",\"en\":\"<p>The game<\\/p>\",\"fr\":\"<p>The game<\\/p>\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:28:06', 'textarea', 1, 0),
(26, 'ACTIVITY content', '{\"ru\":\"<p>The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd. The Cheshire Cat has come to London to test if you, like Alice, can make sense of absurdity in the world around you. Your challenge is to find him on an adventure into the strange, surreal and absurd.<\\/p>\",\"en\":\"<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad&nbsp;<\\/p>\",\"fr\":\"<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad<\\/p>\\r\\n<p>asdsad&nbsp;<\\/p>\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:55', 'textarea', 1, 0),
(27, 'ACTIVITY 2 before', '{\"ru\":\"мероприятия\",\"en\":\"мероприятия\",\"fr\":\"мероприятия\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:55', 'text', 1, 0),
(28, 'ACTIVITY 2 title', '{\"ru\":\"<p>корпоративы<\\/p>\",\"en\":\"<p>корпоративы<\\/p>\",\"fr\":\"<p>корпоративы<\\/p>\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:55', 'textarea', 1, 0),
(29, 'ACTIVITY 2 content', '{\"ru\":null,\"en\":null,\"fr\":null}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:47', 'textarea', 1, 0),
(30, 'ACTIVITY 3 before', '{\"ru\":\"мероприятия\",\"en\":\"мероприятия\",\"fr\":\"мероприятия\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:55', 'text', 1, 0),
(31, 'ACTIVITY 3 title', '{\"ru\":\"<p>дни <span style=\\\"text-decoration: line-through;\\\">рождения<\\/span><\\/p>\",\"en\":\"<p>дни рождения<\\/p>\",\"fr\":\"<p>дни рождения<\\/p>\"}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:30:29', 'textarea', 1, 0),
(32, 'ACTIVITY 3 content', '{\"ru\":null,\"en\":null,\"fr\":null}', 'МЕРОПРИЯТИЯ', '2020-07-30 03:25:15', '2020-07-30 03:25:47', 'textarea', 1, 0),
(33, 'title before', '{\"ru\":\"The game\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(34, 'video btn', '{\"ru\":\"Смотреть видео\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(35, 'Date a title', '{\"ru\":\"Select a date\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(36, 'Date a after', '{\"ru\":null,\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(37, 'select date', '{\"ru\":\"Выберите из свободных дат\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(38, 'date from and to', '{\"ru\":\"Дата\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(39, 'Team Title', '{\"ru\":\"Create team\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(40, 'Team content', '{\"ru\":\"asdasd\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(41, 'Team people name select', '{\"ru\":\"Количество игроков\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(42, 'Team Name input', '{\"ru\":\"Team Name\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(43, 'Captain First Name input', '{\"ru\":\"Captain First Namee\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(44, 'Captain Last Name input', '{\"ru\":\"Captain Last Name\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(45, 'Mobile Number input', '{\"ru\":\"Mobile Number\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(46, 'Contact Email input', '{\"ru\":\"Contact Email\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(47, 'add teams btn', '{\"ru\":\"Добавить команду\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(48, 'Your teams Title', '{\"ru\":\"Your teams\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(49, 'Your teams btn', '{\"ru\":\"Забронировать\",\"en\":null,\"fr\":null}', 'Игра', '2020-08-05 04:53:37', '2020-08-05 04:54:05', 'text', 1, 0),
(50, 'Form Name', '{\"ru\":\"Your final details\",\"en\":\"Your final details\",\"fr\":\"Your final details\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(51, 'Form game select name', '{\"ru\":\"Game Selected\",\"en\":\"Game Selected\",\"fr\":\"Game Selected\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(52, 'Form game date and time', '{\"ru\":\"Date and Time\",\"en\":\"Date and Time\",\"fr\":\"Date and Time\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(53, 'Form game date start name', '{\"ru\":\"Start your game after\",\"en\":\"Start your game after\",\"fr\":\"Start your game after\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(54, 'Form game Number of teams', '{\"ru\":\"Number of teams\",\"en\":\"Number of teams\",\"fr\":\"Number of teams\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(55, 'Form game Total number of players', '{\"ru\":\"Total number of players\",\"en\":\"Total number of players\",\"fr\":\"Total number of players\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(56, 'Form gameTotal Cost', '{\"ru\":\"Total Cost\",\"en\":\"Total Cost\",\"fr\":\"Total Cost\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(57, 'Form select one title', '{\"ru\":\"Have you played HiddenCity before?*\",\"en\":\"Have you played HiddenCity before?*\",\"fr\":\"Have you played HiddenCity before?*\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(58, 'Form select one option', '{\"ru\":null,\"en\":null,\"fr\":null}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:38', 'text', 1, 0),
(59, 'Form select two title', '{\"ru\":\"How did you hear about The Hunt for the Cheshire Cat?*\",\"en\":\"How did you hear about The Hunt for the Cheshire Cat?*\",\"fr\":\"How did you hear about The Hunt for the Cheshire Cat?*\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(60, 'Form select two option', '{\"ru\":\"asdasdas;asdasd\",\"en\":\"asdasdas\",\"fr\":\"asdasdas\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(61, 'Form checkbox one name', '{\"ru\":\"I want to be invited to HiddenCity experiences.\",\"en\":\"I want to be invited to HiddenCity experiences.\",\"fr\":\"I want to be invited to HiddenCity experiences.\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(62, 'Form checkbox two name', '{\"ru\":\"I have been provided with the privacy policy.*\",\"en\":\"I have been provided with the privacy policy.*\",\"fr\":\"I have been provided with the privacy policy.*\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0),
(63, 'Form btn bay name', '{\"ru\":\"Оплатить\",\"en\":\"Оплатить\",\"fr\":\"Оплатить\"}', 'Игра Форма', '2020-08-05 04:53:37', '2020-08-13 12:28:43', 'text', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_stocks`
--

CREATE TABLE `cicada_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pattern` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_subscriptions`
--

CREATE TABLE `cicada_subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_subscription_items`
--

CREATE TABLE `cicada_subscription_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subscription_id` bigint(20) UNSIGNED NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_users`
--

CREATE TABLE `cicada_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_confirm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_users`
--

INSERT INTO `cicada_users` (`id`, `name`, `email`, `tel`, `username`, `email_verified_at`, `password`, `code`, `is_confirm`, `remember_token`, `created_at`, `updated_at`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`) VALUES
(1, 'Олжас', 'fovarit38@gmail.com', NULL, 'admin', NULL, '$2y$10$MwKo1BrmOGCI.RwUskWbR.G6kHgfnznqZEKKzpCg527QKVCVefBR6', NULL, '0', 'Mwt8haUiJ8zGkPtfePxcKrqtPFpYwJx0O9Utfp9FDtl4TiNZaAK1i5nQAanc', '2020-02-02 12:12:09', '2020-02-02 12:12:09', NULL, NULL, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cicada_available_times`
--
ALTER TABLE `cicada_available_times`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_catalogs`
--
ALTER TABLE `cicada_catalogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cicada_catalogs__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Индексы таблицы `cicada_cities`
--
ALTER TABLE `cicada_cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_column_names`
--
ALTER TABLE `cicada_column_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cicada_column_names_name_key_unique` (`name_key`);

--
-- Индексы таблицы `cicada_dat_times`
--
ALTER TABLE `cicada_dat_times`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_failed_jobs`
--
ALTER TABLE `cicada_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_faqs`
--
ALTER TABLE `cicada_faqs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_filters`
--
ALTER TABLE `cicada_filters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_filter_attitudes`
--
ALTER TABLE `cicada_filter_attitudes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_filter_items`
--
ALTER TABLE `cicada_filter_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_languages`
--
ALTER TABLE `cicada_languages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_migrations`
--
ALTER TABLE `cicada_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_model_lists`
--
ALTER TABLE `cicada_model_lists`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_model_metas`
--
ALTER TABLE `cicada_model_metas`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_nav_menus`
--
ALTER TABLE `cicada_nav_menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_news_items`
--
ALTER TABLE `cicada_news_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_orders`
--
ALTER TABLE `cicada_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_order_items`
--
ALTER TABLE `cicada_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_password_resets`
--
ALTER TABLE `cicada_password_resets`
  ADD KEY `cicada_password_resets_email_index` (`email`);

--
-- Индексы таблицы `cicada_permission_dates`
--
ALTER TABLE `cicada_permission_dates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_permission_grups`
--
ALTER TABLE `cicada_permission_grups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_permission_users`
--
ALTER TABLE `cicada_permission_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_products`
--
ALTER TABLE `cicada_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_product_prices`
--
ALTER TABLE `cicada_product_prices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_questions`
--
ALTER TABLE `cicada_questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_shops`
--
ALTER TABLE `cicada_shops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_sites`
--
ALTER TABLE `cicada_sites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_static_texts`
--
ALTER TABLE `cicada_static_texts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_stocks`
--
ALTER TABLE `cicada_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_subscriptions`
--
ALTER TABLE `cicada_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cicada_subscriptions_user_id_stripe_status_index` (`user_id`,`stripe_status`);

--
-- Индексы таблицы `cicada_subscription_items`
--
ALTER TABLE `cicada_subscription_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cicada_subscription_items_subscription_id_stripe_plan_unique` (`subscription_id`,`stripe_plan`),
  ADD KEY `cicada_subscription_items_stripe_id_index` (`stripe_id`);

--
-- Индексы таблицы `cicada_users`
--
ALTER TABLE `cicada_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cicada_users_email_unique` (`email`),
  ADD UNIQUE KEY `cicada_users_tel_unique` (`tel`),
  ADD UNIQUE KEY `cicada_users_username_unique` (`username`),
  ADD KEY `cicada_users_stripe_id_index` (`stripe_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cicada_available_times`
--
ALTER TABLE `cicada_available_times`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `cicada_catalogs`
--
ALTER TABLE `cicada_catalogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_cities`
--
ALTER TABLE `cicada_cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_column_names`
--
ALTER TABLE `cicada_column_names`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `cicada_dat_times`
--
ALTER TABLE `cicada_dat_times`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT для таблицы `cicada_failed_jobs`
--
ALTER TABLE `cicada_failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_faqs`
--
ALTER TABLE `cicada_faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `cicada_filters`
--
ALTER TABLE `cicada_filters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_filter_attitudes`
--
ALTER TABLE `cicada_filter_attitudes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_filter_items`
--
ALTER TABLE `cicada_filter_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_languages`
--
ALTER TABLE `cicada_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `cicada_migrations`
--
ALTER TABLE `cicada_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT для таблицы `cicada_model_lists`
--
ALTER TABLE `cicada_model_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `cicada_model_metas`
--
ALTER TABLE `cicada_model_metas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;

--
-- AUTO_INCREMENT для таблицы `cicada_nav_menus`
--
ALTER TABLE `cicada_nav_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_news_items`
--
ALTER TABLE `cicada_news_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_orders`
--
ALTER TABLE `cicada_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `cicada_order_items`
--
ALTER TABLE `cicada_order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `cicada_permission_dates`
--
ALTER TABLE `cicada_permission_dates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_permission_grups`
--
ALTER TABLE `cicada_permission_grups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `cicada_permission_users`
--
ALTER TABLE `cicada_permission_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_products`
--
ALTER TABLE `cicada_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `cicada_product_prices`
--
ALTER TABLE `cicada_product_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `cicada_questions`
--
ALTER TABLE `cicada_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_shops`
--
ALTER TABLE `cicada_shops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_sites`
--
ALTER TABLE `cicada_sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_static_texts`
--
ALTER TABLE `cicada_static_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT для таблицы `cicada_stocks`
--
ALTER TABLE `cicada_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_subscriptions`
--
ALTER TABLE `cicada_subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_subscription_items`
--
ALTER TABLE `cicada_subscription_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_users`
--
ALTER TABLE `cicada_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
