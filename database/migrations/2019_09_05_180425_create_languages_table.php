<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('name_key');
            $table->integer('visable')->default(0);
            $table->string('alternative')->nullable();
            $table->timestamps();
        });

        \App\Language::insert(
            array(
                'name' => 'rus',
                'name_key' => 'ru',
                'visable' => '1',
                'alternative' => ''
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
