<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->integer('price_old')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('price')->nullable();
            $table->integer('length')->nullable();
            $table->string('ProductsPrice_nomenclature')->nullable();
            $table->string('Product_nomenclature')->nullable();
            $table->string('status')->nullable();
            $table->integer('orders_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
