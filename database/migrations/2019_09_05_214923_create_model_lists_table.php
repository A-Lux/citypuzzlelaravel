<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('name_key')->nullable();
            $table->string('icon')->nullable();
            $table->timestamps();
        });



        $model =  \App\Model_list::insert(
            [
                ["name_key"=>"Catalog","name"=>"Каталог"],
                ["name_key"=>"Product","name"=>"Продукция"],
                ["name_key"=>"Product_price","name"=>"Цена продукции"],
                ["name_key"=>"Filter","name"=>"Названия Фильтров"],
                ["name_key"=>"Filter_item","name"=>"Пункты Фильтров"],
                ["name_key"=>"Filter_attitudes","name"=>"Связи Фильтров"],
                ["name_key"=>"Order","name"=>"Информация о заказе"],
                ["name_key"=>"Order_item","name"=>"Информация о заказе"],
                ["name_key"=>"User","name"=>"Пользователи"],
                ["name_key"=>"Site","name"=>"Сайты"],
                ["name_key"=>"City","name"=>"Города"],
                ["name_key"=>"Shop","name"=>"Магазины"],
                ["name_key"=>"Language","name"=>"Языки"],
            ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_lists');
    }
}
