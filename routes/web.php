<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = Request::segment(1);
$lang = true;

Route::group(['prefix' => url_routes() . '/admin/'], function () {
    Auth::routes();
    Route::get('', 'admin\MainController@index');
    Route::get('dev/{dev_status}', 'admin\MainController@developer');
    Route::get('table', 'admin\TablesController@index');
    Route::post('table_meta_update', 'admin\TablesController@table_meta_update');
    Route::get('table/{model_name}', 'admin\TablesController@edit');
    Route::get('column_name', 'admin\TablesController@column');
    Route::get('column_name/edit/{id}', 'admin\TablesController@column_edit');
    Route::post('update', 'admin\UpdateController@update_model');

    Route::group(['prefix' => 'orders/'], function () {
        Route::post('edit/{id}/updateStatus', 'admin\MainController@status_set');
    });

    Route::post('model/{model_name}', 'admin\GenerationController@model_position');
    Route::get('model/{model_name}', 'admin\GenerationController@model_catalog');
    Route::get('model/{model_name}/{id}', 'admin\GenerationController@model_save');

    Route::get('StaticText', 'admin\MainController@s_text');
    Route::post('update_text', 'admin\MainController@update_text');
});

Route::any('/game/bay/success', 'client\MainController@game_success');
Route::any('/game/bay/cancel', 'client\MainController@game_cancel');

Route::group(['prefix' => url_routes() . '/'], function () {
    Auth::routes();
    Route::get('', 'client\MainController@index');
    Route::get('game/{path}', 'client\MainController@game');
    Route::post('game/time', 'client\MainController@get_time');
    Route::get('activity', 'client\MainController@activity');
    Route::post('order/new', 'client\MainController@order_new');
    Route::get('faq', 'client\MainController@faq');
    Route::post('game/timecalendar_data', 'client\MainController@get_day');
});
