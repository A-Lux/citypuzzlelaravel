<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name', 'name_key', 'sort', 'visable','sites_name_key'
    ];

}
