<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use App;
use DB;
use Stripe;

class MainController extends Controller
{

    public function __construct()
    {
        $url = explode("/", url_custom('/'));
        App::setLocale($url[1]);


    }

    public function index()
    {
        return view('views.main');
    }

    public function game($path)
    {
        $game = \App\Product::where("path", $path)->first();


        return view('views.game', compact('game'));
    }


    public function get_time(Request $request)
    {
        $request = $request->all();

        $products = \App\Product::find($request["id"]);
        $days = \App\Product_price::where("product_id", $products->id)->first();

        $dateList = \App\DatTime::where("date", $request["date"])->where("product_pric_id", $days->id)->first();

        $available = \App\AvailableTime::where("product_price_id", $days->id)->orderby("time")->get();

        $timeAvailable = array_keys($available->groupby("time")->toarray());
//        $days = \App\Product_price::where("product_id", $products->id)->where("date",$request["date"])->get()->toarray();
        foreach ($timeAvailable as $indekx => $tim) {
            $timeAvailable[$indekx] = \Carbon\Carbon::createFromFormat('H:i:s', $tim)->format('H:i');
        }

        $timedate = array_keys(\App\Order::where("date", $request["date"])->wherein("time", $timeAvailable)->get()->groupby("time")->toarray());

        if (!is_null($dateList)) {
            $html = '<input type="hidden" name="date" value="' . date("Y-m-d", strtotime($dateList->date)) . '">';

            foreach ($available as $timout) {
                $timeboxs = \Carbon\Carbon::createFromFormat('H:i:s', $timout->time)->format('H:i');
                if (!in_array($timeboxs, $timedate)) {
                    $html .= '<label class="radio_t">  <input type="radio" name="time"  data-timeid="' . $timout->id . '" class="radio_t_time"  value="' . $timeboxs . '" required> <span class="time_a">' . \Carbon\Carbon::createFromFormat('H:i:s', $timout->time)->format('H:i') . '</span> </label>';
                }
            }
        }
        return $html;
    }

    public function get_day(Request $request)
    {
        $request = $request->all();
        $json_date = json_decode($request["current"], true);
        $calendar = \App\Product_price::where("Product_id", $request["id"])->get();


        $return_date = [];
        foreach ($calendar as $date) {

            $datetime = \App\DatTime::whereIn(DB::raw("DATE(date)"), $json_date)->where("product_pric_id", $date->id)->get();

            foreach ($datetime as $datsd) {


                $timedate = array_keys(\App\Order::where("date", date("Y-m-d", strtotime($datsd->date)))->where("game_id", $request["id"])->get()->groupby("time")->toarray());
                $time_orgnl = \App\AvailableTime::where("product_price_id", $date->id)->count();
                $time = \App\AvailableTime::where("product_price_id", $date->id)->wherenotin("time", $timedate)->count();

                if ($time_orgnl > 0) {
                    array_push($return_date, ["disable" => $time == 0 ? 1 : 0, "date" => date("Y-m-d", strtotime($datsd->date))]);
                }

            }


        }


        return response()->json($return_date);

    }

    public function activity()
    {
        return view('views.activity');
    }

    public function faq()
    {
        return view('views.faq');
    }

    public function order_new(Request $request)
    {
        $request = $request->all();
        $teamInfo = json_decode($request["team_info"], true);

        $players = 0;
        $game = \App\Product::find($request["game_id"]);
//        $day = \App\Product_price::where("Product_id", $game->id)->where("date", $request["date"]["date"])->first();
//        $time = \App\AvailableTime::where("product_price_id", $day->id)->where("time", $request["date"]["time"])->first();

        $code = rand(1000, 999999);
        $price = 0;
        $order = null;
        if (!is_null($game)) {


            foreach ($teamInfo as $tm) {
                $players += $tm["team_people"];
            }
            $order = new \App\Order;
            $order->teamInfo = $request["team_info"];
            $order->time = $request["date"]["time"];
            $order->date = $request["date"]["date"];
            $order->price = $players * (float)$game->price;
            $order->game_id = $request["game_id"];
            $order->order_number = $code;
            $order->time_id = 0;
            $order->status = "new";
            $order->save();

            $price = $order->price;
//            $time->disable = 1;
//            $time->save();

//            if (\App\AvailableTime::where("product_price_id", $day->id)->where("disable", "0")->count() == 0) {
//                $day->disable = 1;
//                $day->save();
//            }


            foreach ($teamInfo as $tm) {
                $team = new \App\Order_item;
                $team->team_name = $tm["team_name"];
                $team->team_people = $tm["team_people"];
                $team->first_name = $tm["first_name"];
                $team->last_name = $tm["last_name"];
                $team->mobile_number = $tm["mobile_number"];
                $team->email = $tm["email"];
                $team->orders_id = $order->id;
                $team->save();
            }

        }


        \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price_data' => [
                    'currency' => 'usd',
                    'product_data' => [
                        'name' => strip_tags(LC($game->title)),
                    ],
                    'unit_amount' => $price * 100,
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => 'https://' . $_SERVER['SERVER_NAME'] . '/game/bay/success?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url' => 'https://' . $_SERVER['SERVER_NAME'] . '/game/bay/cancel?session_id={CHECKOUT_SESSION_ID}',
        ]);

        if (!is_null($order)) {
            $order->session_id = $session->id;
            $order->save();
        }


        return response()->json(['session_id' => $session->id]);
    }


    public function game_success(Request $request)
    {
        $request = $request->all();

        $order_sesion = \App\Order::where("session_id", $request["session_id"])->first();
        $order_sesion->bay_online = 1;
        $order_sesion->save();

        $game = \App\Product::find($order_sesion->game_id);
        $send_html = "<b>Новая запись</b> \n";
        $send_html .= "price: " . $order_sesion->price . " \n";
        $send_html .= "order: " . $order_sesion->order_number . " \n";
        $send_html .= "игра: " . strip_tags(LC($game->title)) . " \n";
        $send_html .= "Дата: " . $game->date . " \n";
        $send_html .= "Время: " . $game->time . " \n";
        if (!is_null($order_sesion)) {
            foreach (json_decode($order_sesion->teamInfo, true) as $keyxs => $telerock) {
                $send_html .= "<b>Команда:</b> №" . ($keyxs + 1) . "\n";
                foreach ($telerock as $keysxss => $telerocsk) {
                    $send_html .= "<b>" . $keysxss . ":</b> " . $telerocsk . "\n";
                }
            }
        }
        bot("1058388746:AAELeUAtJF8Uk2qX2gc4iHwxbp4K1VYiqYE", "-422653903", $send_html);

        return redirect()->to(url_custom(''))->with("alert", 'Спасибо, ваша заявка принята. <br> код вашей брони <b>' . $order_sesion->order_number . '</b>');

    }

    public function game_cancel(Request $request)
    {
        $request = $request->all();
        DB::table("logs")->insert(["log" => json_encode($request)]);

        $order = \App\Order::where("session_id", $request["session_id"])->first();
        $order->status = "cancelled";
        $order->save();
        $available = \App\AvailableTime::find($order->time_id);
        $available->disable = 0;
        $available->save();
        if (\App\AvailableTime::where("product_price_id", $available->product_price_id)->where("disable", "0")->count() > 0) {
            $day = \App\Product_price::find($available->product_price_id);
            $day->save();
        }

        return redirect()->to('/');
    }

}
