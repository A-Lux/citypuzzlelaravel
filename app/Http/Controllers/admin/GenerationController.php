<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use DB;
use Schema;

class GenerationController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function model_position($model_name, Request $request)
    {
        $request = $request->all();
        $model = app("\App\\" . $model_name)->find($request["id"]);
        $model->sort = $request["position"];
        $model->save();
    }


    public function model_catalog($model_name)
    {

        $model = app("\App\\" . $model_name);
        $columns = Schema::getColumnListing($model->getTable());

        if (in_array("sort", $columns)) {
            $model = $model->orderby("sort");
        }
        if ($model_name == "Order") {
            $model = $model->where("bay_online", "1");
        }
        $tbody = $model->get();

        $thead = [];

        $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

        foreach ($meta_colum as $rows) {

            $colums = \App\Column_name::where("name_key", $rows->name_key)->first();

            $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
        }
        $table_link = ["/admin/model/" . $model_name . "/", "id"];

        return view('views.generation.tables_catalog', compact("model_name", "tbody", "thead", "table_link"));
    }

    public function model_save($model_name, $id)
    {
        $model_db = null;
        $model = null;
        if ($model_name != "table") {
            $model = app("\App\\" . $model_name);
            $model = $model->find($id);
            $model_db = \App\Model_list::where("name_key", $model_name)->first();
        }

        return view('views.generation.tables_save', compact("id", "model_db", "model", "model_name"));
    }

}
