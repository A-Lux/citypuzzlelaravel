<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use TableClass;

class MainController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        return view('views.main');
    }


    public function table()
    {
        return view('main');
    }


    public function developer($status)
    {
        if ($status != "true") {
            $status = false;
        }
        Cache::forever('dev', $status);


        return redirect()->back();
    }


    public function orders()
    {
        return view('views.orders');
    }

    public function s_text()
    {
        $model_name = "StaticText";
        $thead_nav = \App\StaticText::get()->groupby("page");
        $tbody = [];

        $page = '';
        if (isset($_REQUEST["page"])) {
            $tbody = \App\StaticText::where("page", $_REQUEST["page"])->get();
            $page = $_REQUEST["page"];
        }

        $thead = ["id" => "id", "name_key" => "name_key"];

        $table_link = ["/admin/model//", "id"];

        return view('views.s_text', compact("model_name", 'page', "thead_nav", "tbody", "thead", "table_link"));

    }

    public function status_set($id, Request $request)
    {

        $order = \App\Order::find($id);
        $order->status = $request["status"];
        $order->save();

        if ($request["status"] == "cancelled") {
            $available = \App\AvailableTime::find($order->time_id);
            $available->disable = 0;
            $available->save();

            if (\App\AvailableTime::where("product_price_id", $available->product_price_id)->where("disable", "0")->count() > 0) {

                $day = \App\Product_price::find($available->product_price_id);

                $day->disable = 0;
                $day->save();
            }
        }

        return redirect()->back();
    }


    public function update_text(Request $request)
    {
        $request = $request->all();

        foreach ($request["save"] as $key => $colum) {
            $s_text = \App\StaticText::where("name_key", $key)->first();
            if (is_array($colum)) {
                $s_text->content = json_encode($colum, JSON_UNESCAPED_UNICODE);
            } else {
                $s_text->content = $colum;
            }
            $s_text->save();
        }
        return redirect()->back();
    }
}
