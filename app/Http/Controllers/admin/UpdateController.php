<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use Schema;
use Str;

class UpdateController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function update_model(Request $request)
    {
        $files = $request;
        $request = $request->all();


        $dateSave = [];
        if ($request["model_name"] == "Product_price") {
            $dateSave = explode(",", $request["date_save"]);
            unset($request["date_save"]);
        }

        foreach ($_FILES as $nameInput => $fileout) {
            if (isset($request[$nameInput])) {
                if ($files->hasFile($nameInput)) {

                    $photo_path = $files->file($nameInput);
                    $type = explode(".", $photo_path->getClientOriginalName());
                    if (count($type) > 1) {
                        $m_path = Str::random(5) . "_file." . $type[1];
                        $moveTo = 'media/Update/' . str_replace("/", "_", $fileout["type"]) . '/';
                        $photo_path->move($moveTo, $m_path);
                        $request[$nameInput] = "/public/" . $moveTo . $m_path;
                        if (isset($request[$nameInput . "_alt"])) {
                            unset($request[$nameInput . "_alt"]);
                        }
                    }
                } else {
                    unset($request[$nameInput]);
                }
            }
        }


        $id = $request["id"];
        $model_name = $request["model_name"];
        $model = null;
        if ($id == 0) {
            $model = app("\App\\$model_name");
        } else {
            $model = app("\App\\$model_name");
            $model = $model->find($id);
        }

        if ($model_name == "Shop") {
            if (!isset($request["product_id_save"])) {
                $request["product_id_save"] = [];
            }
        }

        if (is_null($model)) {
            return redirect()->back()->with("alert", "Модель не найденна");
        }


        foreach ($request as $key => $input) {
            $input_save = explode("_sa", str_replace("_alt", "", $key));
            $is_save = end($input_save);
            if ("sa" . $is_save == "save") {
                if (is_array($input)) {
                    $model->{$input_save[0]} = json_encode($input, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->{$input_save[0]} = $input;
                }
            }
        }

        $model->save();


        $columns = Schema::getColumnListing($model->getTable());
        if (in_array("path", $columns) && in_array("id", $columns) && in_array("slug", $columns)) {
            $columSlug = "";
            if (in_array("title", $columns)) {
                $columSlug = "title";
            } else if (in_array("name", $columns)) {
                $columSlug = "name";
            }
            if ($columSlug != "") {
                $urlSlug = Urlcode(LC($model->{$columSlug}, "ru"));
                $firstEditSlug = true;
                while (!is_null(\app("\App\\$model_name")->where("slug", $urlSlug)->first())) {
                    $urlSlug .= ($firstEditSlug ? "-" : "") . $model->id;
                    $firstEditSlug = false;
                }
                if ($urlSlug != "") {
                    $model->slug = $urlSlug;
                    $model->path = $model->slug;
                    $model->save();
                }
            }
        }


        if ($request["model_name"] == "AvailableTime") {

            if ($request["id"] == 0) {
                $day_save = \App\Product_price::find($request["product_price_id_save"]);
                $day_save->save();
            }
        }

        if ($request["model_name"] == "Product_price") {
            \App\DatTime::where("product_pric_id", $model->id)->delete();
            foreach ($dateSave as $timedate) {
                $day_time = new \App\DatTime;
                $day_time->date = date('Y-m-d', strtotime($timedate));
                $day_time->product_pric_id = $model->id;
                $day_time->save();
            }
            unset($request["date_save"]);
        }

        return redirect()->to(str_replace("{id}", $model->id, ($request["path"])));
    }


    public function status_set($id, Request $request)
    {

    }

}
