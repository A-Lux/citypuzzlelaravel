<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


//    protected function authenticated(Request $request, $user)
//    {
//
//        if (\App\Permission_user::check() !== -1) {
//            return redirect()->to(language_dir() . "/admin");
//        }
//    }


    public function username()
    {
        $nameInput = "login";
        $identity = request()->get($nameInput);
        if (filter_var($identity, FILTER_VALIDATE_EMAIL)) {
            $fieldName = "email";
        } else {
            $fieldName = validate_phone_number($identity) == false ? 'username' : 'tel';
        }
        if ($fieldName == "tel") {
            $identity = validate_phone_number($identity);
        }

        request()->merge([$fieldName => $identity]);

        return $fieldName;
    }



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
