<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Request;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }


    protected function unauthenticated($request, AuthenticationException $exception)
    {


        $path = "";
        if (is_admin_view()) {

            $path = url_custom('/admin/login', true);
        } else {
            $path = url_custom('/login', true);
        }

        return $request->expectsJson()
            ? response()->json(['message' => $exception->getMessage()], 401)
            : redirect()->guest($path);

    }


    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            if ($exception->getMessage() != "Unauthenticated.") {
                if ($exception->getStatusCode() == 404) {
                    $path = str_replace($request->root(), "", $request->fullUrl());
                    $path_array = array_values(array_diff(explode("/", $path), array('')));
                    $path_return = array_values(array_diff(explode("/", $path), array('')));

                    $language = \App\Language::get();
                    $languageSing = $language->first();
                    $cities = \App\City::get();
                    $citySing = $cities->first();

                    $controlFalse = false;
                    $path_current = [];

                    if ($path == "") {
                        return redirect()->to("/fr/" . $citySing->name_key);
                    }


                    foreach ($language as $index_key => $lang) {
                        if (isset($path_array[0])) {
                            if ($path_array[0] == $lang->name_key) {
                                $path_current[0] = $lang->name_key;
                                break;
                            } else if ($index_key == count($language) - 1) {
                                $path_current[0] = false;
                            }
                        }
                    }

                    foreach ($cities as $index_key => $citi) {
                        if (isset($path_array[1])) {
                            if ($path_array[1] == $citi->name_key) {
                                $path_current[1] = $citi->name_key;
                                break;
                            } else if ($index_key == count($cities) - 1) {
                                $path_current[1] = false;
                            }
                        }
                    }


                    if ($path_current[0] == false) {
                        $path_current[0] = $languageSing->name_key;
                        $controlFalse = true;
                    } else {
                        unset($path_return[0]);
                    }

                    if (isset($path_current[1]) ? ($path_current[1] == false) : true) {
                        $path_current[1] = $citySing->name_key;
                        $controlFalse = true;
                    } else {
                        unset($path_return[1]);
                    }

                    if ($controlFalse) {
                        $pathOut = "/" . implode("/", $path_current) . (count($path_return) > 0 ? '/' : '') . implode("/", $path_return);
                        return redirect()->to($pathOut);
                    } else {
                        return parent::render($request, $exception);
                    }
                }
            }
        }
        return parent::render($request, $exception);
    }
}
