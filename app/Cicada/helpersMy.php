<?php


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

function column_rename($name_key)
{

    $column = \App\Column_name::where("name_key", $name_key)->first();

    if (!is_null($column)) {
        return $column->name;
    }
    return $name_key;

}

function bot($api, $chat_id, $message)
{
    $apiToken = $api;
    $reply_markup = [];
    //$reply_markup=json_encode(array('inline_keyboard' => array(array(array('text' => 'Просмотреть заявку', 'url' => Request::root() . '/submit/' . $code)))));

    $data = [
        'chat_id' => $chat_id,
        'text' => $message,
        'parse_mode' => 'HTML',
        'reply_markup' => $reply_markup
    ];
    $response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data));
}

function url_routes()
{
    try {
        return language_dir() . city_dir();
    } catch (Exception $e) {
        return '/';
    }
    return '/';
}

function language_dir()
{


    $path = str_replace(Request::root(), "", Request::fullUrl());
    $path_array = array_values(array_diff(explode("/", $path), array('')));
    $language = \App\Language::get();
    $languageSing = $language->first();
    $path_current = [];

    foreach ($language as $index_key => $lang) {
        if (isset($path_array[0])) {

            if ($path_array[0] == $lang->name_key) {

                $path_current[0] = $lang->name_key;
                break;
            } else if ($index_key == count($language) - 1) {
                $path_current[0] = $languageSing->name_key;
            }
        } else {
            $path_current[0] = $languageSing->name_key;
        }
    }

    return '/' . $path_current[0];
}

function city_dir()
{
    $cities = \App\City::get();
    $citySing = $cities->first();
    $path = str_replace(Request::root(), "", Request::fullUrl());
    $path_array = array_values(array_diff(explode("/", $path), array('')));
    foreach ($cities as $index_key => $citi) {
        if (isset($path_array[1])) {
            if ($path_array[1] == $citi->name_key) {
                $path_current[1] = $citi->name_key;
            } else if ($index_key == count($cities) - 1) {
                $path_current[1] = $citySing->name_key;
            }
        } else {
            $path_current[1] = $citySing->name_key;
        }
    }

    return '/' . $path_current[1];
}

function is_admin_view()
{
    if (isset($_SERVER['REQUEST_URI'])) {
        $adminType = explode("?", $_SERVER['REQUEST_URI']);
        $adminType = explode("/", $adminType[0]);

        if (in_array("admin", $adminType)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function upload($foto)
{
    $imageName = Str::random(20) . ".jpg";

    $imagen = Image::make($foto)->encode('jpg', 75);
    Storage::disk('dropbox')->put('imagenes/' . $imageName, $imagen->stream()->__toString());

    $dropbox = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();
    $response = $dropbox->createSharedLinkWithSettings('imagenes/' . $imageName);

    $url = str_replace("dl=0", "raw=1", $response["url"]);
    $headers = get_headers($url, 1);

    return $headers['Location'][1];
//    ["resolved_visibility" => "public"]
}


function validate_phone_number($phone)
{
    $phone = str_replace("-", "", filter_var($phone, FILTER_SANITIZE_NUMBER_INT));

    if (mb_substr($phone, 0, 1) == "8") {
        $phone = mb_substr($phone, 1, mb_strlen($phone));
    } else if (mb_substr($phone, 0, 1) == "+") {
        $phone = mb_substr($phone, 2, mb_strlen($phone));
    }

    $phone = preg_replace("/[^0-9]/", '', $phone);

    if (strlen($phone) != 10) {
        return false;
    } else {
        return $phone;
    }
}

function url_custom_lang($lang)
{


    $path = explode("/", str_replace(Request::root(), "", Request::fullUrl()));
    $path[1] = $lang;
    return implode('/', $path);

}


function url_custom($path, $full = false)
{

    if ($full == true) {
        return url("/" . Request::segment(1) . "/" . Request::segment(2) . $path);
    } else {
        return "/" . Request::segment(1) . "/" . Request::segment(2) . $path;
    }


}

function calculate_age($birthday)
{
    $birthday_timestamp = strtotime($birthday);
    $age = date('Y') - date('Y', $birthday_timestamp);
    if (date('md', $birthday_timestamp) > date('md')) {
        $age--;
    }
    switch (substr($age, -1)) {
        case 1:
            return $age . " год";
            break;
        case 2:
            return $age . " года";
            break;
        case 3:
            return $age . " года";
            break;
        case 4:
            return $age . " года";
            break;
        case 5:
            return $age . " лет";
            break;
        case 6:
            return $age . " лет";
            break;
        case 7:
            return $age . " лет";
            break;
        case 8:
            return $age . " лет";
            break;
        case 9:
            return $age . " лет";
            break;
        case 0:
            return $age . " лет";
            break;
    }
}

function calculate_day($day)
{
    if ($day == 1) {
        echo "$day день";
    } elseif (substr($day, -1) == 2) {
        echo "$day дня";
    } elseif (substr($day, -1) == 3) {
        echo "$day дня";
    } elseif (substr($day, -1) == 4) {
        echo "$day дня";
    } else {
        echo "$day дней";
    }
}

function rus_date($date)
{

    $translate = array(
        "am" => "дп",
        "pm" => "пп",
        "AM" => "ДП",
        "PM" => "ПП",
        "Monday" => "Понедельник",
        "Mon" => "Пн",
        "Tuesday" => "Вторник",
        "Tue" => "Вт",
        "Wednesday" => "Среда",
        "Wed" => "Ср",
        "Thursday" => "Четверг",
        "Thu" => "Чт",
        "Friday" => "Пятница",
        "Fri" => "Пт",
        "Saturday" => "Суббота",
        "Sat" => "Сб",
        "Sunday" => "Воскресенье",
        "Sun" => "Вс",
        "January" => "Января",
        "Jan" => "Янв",
        "February" => "Февраля",
        "Feb" => "Фев",
        "March" => "Марта",
        "Mar" => "Мар",
        "April" => "Апреля",
        "Apr" => "Апр",
        "May" => "Мая",
        "June" => "Июня",
        "Jun" => "Июн",
        "July" => "Июля",
        "Jul" => "Июл",
        "August" => "Августа",
        "Aug" => "Авг",
        "September" => "Сентября",
        "Sep" => "Сен",
        "October" => "Октября",
        "Oct" => "Окт",
        "November" => "Ноября",
        "Nov" => "Ноя",
        "December" => "Декабря",
        "Dec" => "Дек",
        "st" => "ое",
        "nd" => "ое",
        "rd" => "е",
        "th" => "ое"
    );

    return str_replace(array_keys($translate), array_values($translate), $date);

}

function amask($arraymask, $arrayin)
{

    foreach ($arraymask as $k => $v) {

        if (!isset($arrayin[$k])) {
            $arrayin[$k] = $v;
        }
    }

    return $arrayin;
}


function LC($data, $lang = "", $edit = false)
{

    $data = str_replace(["\n", "\r", "\t"], "", $data);
    $dataIsArray = json_decode(htmlspecialchars_decode($data, ENT_QUOTES), true);

    $language = App::getLocale();


    if ($lang != "") {
        $language = $lang;
    }


    if (is_array($dataIsArray)) {
        if (isset($dataIsArray[$language]) ? (str_replace([" ", "&nbsp;", "&gt;"], "", strip_tags($dataIsArray[$language])) == "" ? false : true) : false) {
            return ($dataIsArray[$language]);
        } else {
            foreach ($dataIsArray as $sinLang) {
                if (str_replace([" ", "&nbsp;", "&gt;"], "", $sinLang) != "") {
                    return $sinLang;
                }
            }
            return "";
        }
    } else {
        if ($language == "ru") {
            return $data;
        } else {
            if ($edit == false) {
                return $data;
            } else {
                return '';
            }
        }
    }
    return $data;
}


function s_($key, $page, $content, $type = "type_input", $lang = 1)
{
    $s_text = \App\StaticText::where("name_key", $key)->first();
    if (is_null($s_text)) {
        $s_text = new \App\StaticText;
        $s_text->content = $content;
        $s_text->page = $page;
        $s_text->name_key = $key;
        $s_text->type_input = $type;
        $s_text->languages = $lang;
        $s_text->save();
    }
    return LC($s_text->content);
}

function maskInput($errors, $option = [], $data = null)
{

    $options = [
        "name" => "",
        "placeholder" => "",
        "type" => "text",
        "lang" => false,
        "value" => "",
        "value_old" => "",
        "auto" => true,
        "attr" => "text",
        "ph" => "",
        "class" => "",
        "required" => false,
    ];


    $str_random = Str::random(6);
    $optionsout = amask($options, $option);


    $name_send = $optionsout["name"];
    $altname = $optionsout["name"];
    $optionsout["name"] = str_Replace("_save", "", $optionsout["name"]);

    if (!is_null($data)) {
        if (isset($data->{$optionsout["name"]})) {
            $optionsout["value"] = $data->{$optionsout["name"]};
        }
    }


    $optionsout["value"] = htmlspecialchars($optionsout["value"], ENT_QUOTES, 'UTF-8');

    if ($optionsout["type"] == "date") {
        if ($optionsout["value"] != "") {
            if ($optionsout["class"] != "multidate") {
                $optionsout["value"] = date('Y-m-d', strtotime($optionsout["value"]));
            }
        }
    }
    if ($optionsout["type"] == "datetime-local") {
//        if ($value != "") {
//            $value = date('Y-m-d\TH:i', strtotime($value));
//        }
//        $addAtrebut = ' min="1900-01-10" ';
    }

    if ($optionsout["type"] == "file") {
        ?>

        <div class="language_grup">

            <div class="language_grup_hid language_grup_multi language_grup_ru active ">
                <?php
                if ($optionsout["placeholder"] != "none") {
                    ?>
                    <label class="label_control" for="<?= $str_random . "sa" ?>"><b
                            class="text_bigs">  <?= $optionsout["placeholder"] ?></b> : multi-lingualism </label>
                    <?php
                }
                ?>
                <div class="form-group form-link filbox"
                     style="<?= $optionsout["placeholder"] == "none" ? 'margin-bottom:0;' : '' ?>">
                    <input type="text" name="<?= str_Replace("_save", "_save_alt", $altname) ?>"
                           class="form-control"
                           id="<?= $str_random ?>" value="<?= $optionsout["value"] ?>"
                           placeholder="<?= $optionsout["ph"] ?>" <?= $optionsout["auto"] == false ? ($optionsout["type"] == "password" ? 'autocomplete="new-password"' : 'autocomplete="off"') : '' ?>>

                    <div class="boxsinvs">
                        <input type="<?= $optionsout["type"] ?>" name="<?= $name_send ?>"
                               class="form-control filecontrols"
                               id="<?= $str_random . "sa" ?>"
                               placeholder="<?= $optionsout["ph"] ?>" <?= $optionsout["auto"] == false ? ($optionsout["type"] == "password" ? 'autocomplete="new-password"' : 'autocomplete="off"') : '' ?>>
                        <label for="<?= $str_random . "sa" ?>" class="boxLImaege">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-file-text">
                                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                <polyline points="14 2 14 8 20 8"></polyline>
                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                <polyline points="10 9 9 9 8 9"></polyline>
                            </svg>
                        </label>
                    </div>
                </div>

            </div>

        </div>


        <?php
    } else if (true) {
        ?>
        <div class="language_grup">
            <?php
            $lang_s = \App\Language::get();
            if ($optionsout["lang"] == 0) {
                $lang_s = \App\Language::where("id", "1")->get();
            }
            ?>
            <div class="language_grup_box <?= count($lang_s) > 1 ? 'transition' : '' ?>"
                 style="width:<?= 100 * count($lang_s) ?>%" data-langcount="<?= count($lang_s) ?>">
                <?php


                $optionsout["value_old"] = $optionsout["value"];
                foreach ($lang_s as $keyxs => $langs) {
                    $optionsout["value"] = LC($optionsout["value_old"], $langs->name_key, true);


                    ?>
                    <div
                        class="language_grup_hid <?= count($lang_s) == 1 ? 'language_grup_multi' : '' ?> language_grup_<?= $langs->name_key ?> <?= $keyxs == 0 ? 'active' : '' ?> ">

                        <?php
                        if ($optionsout["placeholder"] != "none") {
                            ?>
                            <label for="basic-url" class="label_control"> <b
                                    class="text_bigs"><?= $optionsout["placeholder"] ?></b>
                                : <?= count($lang_s) == 1 ? 'multi-lingualism' : $langs->name_key ?> </label>
                            <?php
                        }
                        ?>

                        <div class="form-group form-link"
                             style="<?= $optionsout["placeholder"] == "none" ? 'margin-bottom:0;' : '' ?>">
                            <?php
                            if ($name_send != "content_save" && $optionsout["type"] != "textarea") {
                                ?>
                                <?php
                                if ($optionsout["class"] == "multidate") {
                                    ?>
                                    <div class="boxClsendar">
                                        <div class="boxClsendar_date">
                                            <textarea class="multipleDate"
                                                      name="<?= $name_send ?><?= count($lang_s) == 1 ? '' : '[' . $langs->name_key . ']' ?>"
                                                      style="display:none;"><?= $optionsout["value"] ?></textarea>
                                            <div class="multidate"></div>
                                        </div>
                                        <div class="boxClsendar_redult">
                                        </div>
                                    </div>
                                    <?
                                } else {
                                    ?>
                                    <input type="<?= $optionsout["type"] ?>"
                                           name="<?= $name_send ?><?= count($lang_s) == 1 ? '' : '[' . $langs->name_key . ']' ?>"
                                           class="form-control <?= $optionsout["class"] ?>"
                                           id="<?= $str_random ?>" value="<?= $optionsout["value"] ?>"
                                           placeholder="<?= $optionsout["ph"] ?>" <?= $optionsout["auto"] == false ? ($optionsout["type"] == "password" ? 'autocomplete="new-password"' : 'autocomplete="off"') : '' ?>>
                                    <?php
                                }
                            } else if ($optionsout["type"] == "textarea") {
                                ?>
                                <textarea rows="5"
                                          name="<?= $name_send ?><?= count($lang_s) == 1 ? '' : '[' . $langs->name_key . ']' ?>"
                                          class="form-control editor"
                                          placeholder="<?= $optionsout["ph"] ?>" <?= $optionsout["auto"] == false ? ($optionsout["type"] == "password" ? 'autocomplete="new-password"' : 'autocomplete="off"') : '' ?>><?= $optionsout["value"] ?></textarea>
                                <?php
                            } else {

                                ?>
                                <textarea rows="5"
                                          name="<?= $name_send ?><?= count($lang_s) == 1 ? '' : '[' . $langs->name_key . ']' ?>"
                                          class="form-control editor"
                                          placeholder="<?= $optionsout["ph"] ?>"<?= $optionsout["auto"] == false ? ($optionsout["type"] == "password" ? 'autocomplete="new-password"' : 'autocomplete="off"') : '' ?>><?= $optionsout["value"] ?></textarea>
                                <?php
                            }
                            ?>
                        </div>

                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php
    } else {

        ?>
        <div class="form-group form-link">
            <input type="<?= $optionsout["type"] ?>" name="<?= $name_send ?>"
                   class="form-control <?= $optionsout["class"] ?>"
                   id="<?= $str_random ?>" value="<?= $optionsout["value"] ?>"
                   placeholder="<?= $optionsout["placeholder"] ?>" <?= $optionsout["auto"] == false ? ($optionsout["type"] == "password" ? 'autocomplete="new-password"' : 'autocomplete="off"') : '' ?>>
        </div>
        <?php
    }


    if ($errors->has($optionsout["name"])) {
        ?>
        <label id="<?= $str_random ?>-error" for="<?= $str_random ?>"
               class="form-text text-muted"><?= $errors->first($optionsout["name"]) ?></label>
        <?php
    }
    ?>
    <?php

}

function GMN($model_name)
{
    $modelNameGet = \App\Model_list::where("name_key", $model_name)->first();
    if (!is_null($modelNameGet)) {
        return $modelNameGet->name;
    }
    return "Нет данных";
}

function setEnvironmentValue($envKey, $envValue)
{
    $envFile = app()->environmentFilePath();
    $str = file_get_contents($envFile);

    $str = str_replace("$envKey='" . env($envKey) . "'", "$envKey={$envValue}", $str);

    $fp = fopen($envFile, 'w');
    fwrite($fp, $str);
    fclose($fp);
}

function Urlcode($code)
{

    // $replacements = array('!', '*'," ", "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]", "\\");

    return Str::slug(strip_tags($code));
}


function get_type_sql()
{

    $numbers = [
        'boolean',
        'tinyint',
        'smallint',
        'mediumint',
        'integer',
        'int',
        'bigint',
        'decimal',
        'numeric',
        'money',
        'float',
        'real',
        'double',
        'double precision',
    ];

    $strings = [
        'char',
        'character',
        'varchar',
        'character varying',
        'string',
        'guid',
        'uuid',
        'tinytext',
        'text',
        'mediumtext',
        'longtext',
        'tsquery',
        'tsvector',
        'xml',
    ];

    $datetime = [
        'date',
        'datetime',
        'year',
        'time',
        'timetz',
        'timestamp',
        'timestamptz',
        'datetimetz',
        'dateinterval',
        'interval',
    ];

    $lists = [
        'enum',
        'set',
        'simple_array',
        'array',
        'json',
        'jsonb',
        'json_array',
    ];

    $binary = [
        'bit',
        'bit varying',
        'binary',
        'varbinary',
        'tinyblob',
        'blob',
        'mediumblob',
        'longblob',
        'bytea',
    ];

    $network = [
        'cidr',
        'inet',
        'macaddr',
        'txid_snapshot',
    ];

    $geometry = [
        'geometry',
        'point',
        'linestring',
        'polygon',
        'multipoint',
        'multilinestring',
        'multipolygon',
        'geometrycollection',
    ];

    return [
        "numbers" => $numbers,
        "strings" => $strings,
        "datetime" => $datetime,
        "lists" => $lists,
        "binary" => $binary,
        "network" => $network,
        "geometry" => $geometry,
    ];
}


?>
