@extends('views.layouts.app')

@section('content')

    <div class="game_head">
        <div class="container">
            <div class="row">
                <div class="col-md-6 game_wrap game_wrap-0">
                    <div class="game_title">
                        <h3>{!!s_("title before","Игра","The game","text")!!}</h3>
                        <h2>{!! (LC($game->title)) !!}</h2>
                        <div class="game_cost">
                            <span class="priceStyle"><span class="prucebox">{{LC($game->price)}}</span>$</span>
                            @if(LC($game->video)!="")
                                <a class="game_video" href="#">{!!s_("video btn","Игра","Смотреть видео","text")!!}</a>
                            @endif
                        </div>
                        {!! LC($game->content) !!}
                    </div>

                </div>
                <div class="col-md-6 game_wrap game_wrap_2">
                    <div class="game_select">
                        <div class="game_select_title">
                            <span>1</span>
                            <h2 class="game_select_h2">{!!s_("Date a title","Игра","Select a date","text")!!}</h2>
                            <p>{!!s_("Date a after","Игра","","text")!!}</p>
                        </div>
                        <div id="app" data-url="{{url_custom('/game/time')}}" data-id="{{$game->id}}"></div>
                        <div class="app_info">
                            <div class="close_day">
                                <img src="/public/media/client/img/close_day.png" alt=""/><span>Забронировано</span>
                            </div>
                            <div class="info_day">
                                <img src="/public/media/client/img/info.png"
                                     alt=""/><span>{!!s_("select date","Игра","Выберите из свободных дат","text")!!}</span>
                            </div>
                        </div>
                        <div class="app_info time_info">
                            <div class="app_time_click">
                                <span>{!!s_("date from and to","Игра","Дата","text")!!}: <i class="result_day"
                                                                                            style=" font-style: normal; ">9 июля 2020 года</i></span>
                            </div>
                            <div class="app_time_click">
                                <a class="time_edit" href="#"><img src="img/edit_icon.png" alt=""/></a><span>Время с <i
                                        class="result_time" style=" font-style: normal; "></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="team_descr" class="team_descr">
        <div class="container">
            <div class="row">
                <div class="col-md-6 team_wrap" style="display: flex;flex-direction: column;">
                    <div class="game_select_title">
                        <span>2</span>
                        <h2>{!!s_("Team Title","Игра","Create team","text")!!}</h2>
                        <div class="team_edit_name">Team 1</div>
                        <p class="game_p">{!!s_("Team content","Игра","","text")!!}</p>
                    </div>
                    <form class='team_form' style=" margin-top: auto; ">
                        <div class="team_inputs">
                            <div class="team_inputs_descr">
                                <h3>{!!s_("Team people name select","Игра","Количество игроков","text")!!}</h3>
                                <select name="team_people" class="team_people" required>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="team_inputs_descr">
                                <h3>{!!s_("Team Name input","Игра","Team Name","text")!!}</h3>
                                <input class="team_name" name="team_name" type="text" placeholder="" required>
                            </div>
                        </div>
                        <div class="team_inputs">
                            <div class="team_inputs_descr">
                                <h3>{!!s_("Captain First Name input","Игра","Captain First Namee","text")!!}</h3>
                                <input class="first_name" name="first_name" type="text" placeholder="" required>
                            </div>
                            <div class="team_inputs_descr">
                                <h3>{!!s_("Captain Last Name input","Игра","Captain Last Name","text")!!}</h3>
                                <input class="last_name" name="last_name" type="text" placeholder="" required>
                            </div>
                        </div>
                        <div class="team_inputs">
                            <div class="team_inputs_descr">
                                <h3>{!!s_("Mobile Number input","Игра","Mobile Number","text")!!}</h3>
                                <input class="mobile_number" name="mobile_number" type="text" placeholder="" required>
                            </div>
                            <div class="team_inputs_descr">
                                <h3>{!!s_("Contact Email input","Игра","Contact Email","text")!!}</h3>
                                <input class="email" type="email" name="email" placeholder="" required>
                            </div>
                        </div>
                        <button class="team_btn"
                                type="submit">{!!s_("add teams btn","Игра","Добавить команду","text")!!}</button>
                    </form>
                </div>
                <div class="col-md-6 team_wrap">
                    <div class="game_select_title">
                        <h2 class="pt_h2">{!!s_("Your teams Title","Игра","Your teams","text")!!}</h2>
                    </div>
                    <div class="team_list">
                        <ul class="list">
                        </ul>
                        <button
                            class="reservation_btn">{!!s_("Your teams btn","Игра","Забронировать","text")!!}</button>
                    </div>
                </div>
            </div>
            <button class="save_edit ">Сохранить изменения</button>
        </div>
    </section>



    <div class="modal fade" id="timeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="submitday" method="post" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Выберите свободное время</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="time_a_wrap">
                        <div class="set_time">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <p class="time_p">Игра длится 1час 30 минут</p>
                    <button class="btn_success " type="submit">Выбрать</button>
                </div>
            </form>
        </div>
    </div>


    <form action="" method="post" class="modal fade" id="payment-form" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalLongTitle"
          aria-hidden="true">
        <input type="hidden" name="game_id" value="{!! ($game->id) !!}">
        <input type="hidden" name="time" id="return_time">
        <div class="modal-dialog" style="max-width: 600px!important;margin-top:8rem;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title reservation_title_modal" style=" width: 100%; " id="exampleModalLongTitle">
                        <div class="game_select_title reservation_title_ml"
                             style=" margin-left: 0; padding: 0; padding: 0 30px; text-align: left;">
                            <h2 style="padding-left: 0;">Form of payment</h2>
                        </div>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="reservation_modal_block">

                        <div class="reservation_wrap" style=" padding: 0 30px; box-sizing: border-box; ">
                            <div class="" style="padding-top: 0rem;color:#fff;">
                                <div>
                                    <div class="form-row" style=" margin: 0; ">
                                        <label id="headcre" for="card-element">
                                            Credit or debit card
                                        </label>
                                        <div id="card-element">
                                            <!-- A Stripe Element will be inserted here. -->
                                        </div>

                                        <!-- Used to display form errors. -->
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer" style=" padding-bottom: 2rem; margin-top: 0.5rem; ">
                    <button type="submit" class="btn_success reservation_btn_modal">Оплатить</button>
                </div>
            </div>
        </div>
    </form>



    <form action="" method="post" class="modal fade" id="reservationModal" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalLongTitle"
          aria-hidden="true">
        <input type="hidden" name="game_id" value="{!! ($game->id) !!}">
        <input type="hidden" name="time" id="return_time">
        <div class="modal-dialog" style="max-width: 600px!important;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title reservation_title_modal" id="exampleModalLongTitle">
                        <div class="game_select_title reservation_title_ml">
                            <span>3</span>
                            <h2>{!!s_("Form Name","Игра Форма","Your final details","text")!!}</h2>
                        </div>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="reservation_modal_block">
                        <div class="reservation_wrap">
                            <h3>{!!s_("Form game select name","Игра Форма","Game Selected","text")!!}</h3>
                            <p>The Hunt for the Cheshire Cat</p>
                        </div>
                        <div class="reservation_wrap">
                            <input type="hidden" id="gane_date" name="date[date]" value="">
                            <input type="hidden" id="gane_time" name="date[time]" value="">
                            <h3>{!!s_("Form game date and time","Игра Форма","Date and Time","text")!!}</h3>
                            <p><span class="result_day"></span>
                                {!!s_("Form game date start name","Игра Форма","Start your game after","text")!!} <span
                                    class="result_time_before"></span>.</p>

                        </div>
                        <div class="reservation_wrap">
                            <h3>{!!s_("Form game Number of teams","Игра Форма","Number of teams","text")!!}</h3>
                            <p id="teamCount">2</p>
                        </div>
                        <div class="reservation_wrap">
                            <h3>{!!s_("Form game Total number of players","Игра Форма","Total number of players","text")!!}</h3>
                            <p class="team_people_all">8</p>
                        </div>
                        <div class="reservation_wrap">
                            <h2>{!!s_("Form gameTotal Cost","Игра Форма","Total Cost","text")!!}: <span
                                    id="team_people_all" class="fullprice">25</span>$</h2>
                        </div>
                        <div class="reservation_wrap">
                            <h4>{!!s_("Form select one title","Игра Форма","Have you played HiddenCity before?*","text")!!}</h4>
                            <div class="select">
                                <select name="played" >
                                    @php
                                        $option_one=explode(";",s_("Form select one option","Игра Форма","","text"));
                                    @endphp
                                    @foreach($option_one as $opri)
                                        @if($opri!="")
                                            <option value="{{$opri}}">{{$opri}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="reservation_wrap">
                            <h4>{!!s_("Form select two title","Игра Форма","How did you hear about The Hunt for the Cheshire Cat?*","text")!!}</h4>
                            <div class="select">
                                <select name="">
                                    @php
                                        $option_two=explode(";",s_("Form select two option","Игра Форма","","text"));
                                    @endphp
                                    @foreach($option_two as $opri)
                                        @if($opri!="")
                                            <option value="{{$opri}}">{{$opri}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="reservation_wrap">
                            <div class="page__toggle">
                                <label class="toggle">
                                    <input class="toggle__input" type="checkbox">
                                    <span class="toggle__label">
                                      <span
                                          class="toggle__text"> {!!s_("Form checkbox one name","Игра Форма","I want to be invited to HiddenCity experiences.","text")!!} </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="reservation_wrap">
                            <div class="page__toggle">
                                <label class="toggle">
                                    <input class="toggle__input" type="checkbox">
                                    <span class="toggle__label">
                                      <span
                                          class="toggle__text">{!!s_("Form checkbox two name","Игра Форма","I have been provided with the privacy policy.*","text")!!}</span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <input class="invisible_input_time" type="hidden" value="">
                <input class="invisible_input_day" type="hidden" value="">
                <input class="invisible_input_year" type="hidden" value="">
                <div class="modal-footer">
                    <button type="submit"
                            class="btn_success reservation_btn_modal">{!!s_("Form btn bay name","Игра Форма","Оплатить","text")!!}</button>
                </div>
            </div>
        </div>
    </form>

    @if(LC($game->video)!="")
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             id="videoModal_game" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Видео</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! $game->video !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
    <style>
        #videoModal_game iframe {
            width: 100% !important;
            min-height: 459px !important;
            margin-top: 30px !important;
        }
    </style>

@endsection



@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>

    <div class="STRIPE_KEY">{{env("STRIPE_KEY")}}</div>

@endsection
