@extends('views.layouts.app')

@section('content')

    <div class="main_header_block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 col-sm-6 col-xl-12 col-lg-12">
                    <div class="faq_descr_activity">
                        <h3>{!!s_("Slider compani name","Главная","city puzzle")!!}</h3>
                        <h2> {!!s_("FAQ title","FAQ","ответы и вопросы","textarea")!!}</h2>
                        <p>{!!s_("FAQ after","FAQ","ответы и вопросы","text")!!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <section class="faq_content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        @foreach(\App\Faq::orderby("sort")->get() as $keyxs=>$faq)
                            <li class="accordion">{{$keyxs+1}}) {!!LC($faq->question) !!}</li>
                            <div class="panel">
                                {!! LC($faq->content) !!}
                            </div>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <style>
        header {
            position: relative !important;
        }
    </style>

@endsection

