<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} </title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/public/media/client/css/style.css?v=0.1">
    <meta name="description" content="">
    <meta name="Keywords" content="">
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:400,600,700&display=swap&subset=cyrillic"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <style>
        .menu {
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 4;
            background-color: #fff;
            visibility: hidden;
            transform: translateX(-100%);
            transition: 0.6s;
        }
    </style>
</head>
<body>


<header style=" padding-top: 16px; position: absolute; left: 0; width: 100%; z-index: 100; ">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-4 col-sm-4 col-xl-4 col-lg-4">
                <header class="offcanvas-menu">
                    <input type="checkbox" id="toogle-menu"/>

                    <label for="toogle-menu" class="toogle-open"><span></span></label>

                    <nav>
                        <div>
                            <label for="toogle-menu" class="toogle-close">
                                <span></span>
                            </label>
                        </div>
                        <ul>
                            <li class="active"><a href="/">Главная </a></li>
                            <li><a href="{{url_custom('/activity')}}">Мероприятия</a></li>
                            <li><a href="{{url_custom('#best_games')}}">Наши игры</a></li>
                            <li><a href="{{url_custom('/faq')}}">FAQ </a></li>
                        </ul>
                        <div class="contact">
                            <a class="contact_us">Связаться с нами</a>
                        </div>
                    </nav>
                </header>
            </div>
            <div class="col-md-4 col-4 col-sm-4 col-xl-4 col-lg-4">
                <a href="/" class="logo">
                    <img src="/public/media/client/img/logo.png" alt="">
                </a>
            </div>
            <div class="col-md-4 col-4 col-sm-4 col-xl-4 col-lg-4">
                <div class="lang_menu">
                    <ul>
                        @php
                            $language = App::getLocale();
                        @endphp
                        @foreach(\App\Language::get() as $leng)
                            <li class="{{$language==$leng->name_key?'bordsa':''}}"><a href="{{url_custom_lang($leng->name_key)}}">{{$leng->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


@yield('content')


<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12 col-sm-12 col-xl-12 col-lg-12">
                <h3 class="email">E-mail</h3>
                <h2>citypuzzle@mail.ru</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-12 col-sm-12 col-xl-4 col-lg-4 cen-sm-text">
                <a class="contact_us">Обратная связь</a>
            </div>
            <div class="col-md-4 col-12 col-sm-12 col-xl-4 col-lg-4">
                <h3 class="copyright">© 2020 Сity Puzzle. Все права защищены.</h3>
            </div>
            <div class="col-md-4 col-4 col-sm-4 col-xl-4 col-lg-4">
                <div class="soc_block">
                    <a href="#"><img src="img/face.png" alt=""></a>
                    <a href="#"><img src="img/insta.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     id="videoModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Видео</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe class="video" width="100%" height="520" src="https://www.youtube.com/embed/qvBX9VgPGSg"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="sertificateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Сертификат</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="modal_p">Для покупки сертификата
                    отправьте нам ваши данные
                    и мы с вами свяжемся</p>
                <div class="input_descr">
                    <h3>Имя</h3>
                    <input type="text" placeholder="Евгений">
                </div>
                <div class="input_descr">
                    <h3>Телефон</h3>
                    <input type="text" placeholder="+7 555 656 20 25">
                </div>
                <div class="input_descr">
                    <h3>Email</h3>
                    <input type="text" placeholder="hardchehelrih@gmail.com|">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn_success">Отправить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tocontactUsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Обратная связь</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input_descr">
                    <h3>Имя</h3>
                    <input type="text" placeholder="Евгений">
                </div>
                <div class="input_descr">
                    <h3>Телефон</h3>
                    <input type="text" placeholder="+7 555 656 20 25">
                </div>
                <div class="input_descr">
                    <h3>Email</h3>
                    <input type="text" placeholder="hardchehelrih@gmail.com|">
                </div>
                <div class="textarea_descr">
                    <h3>Текст</h3>
                    <textarea name="" id="" cols="10" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn_success">Отправить</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="sertificateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Сертификат</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="modal_p">Для покупки сертификата
                    отправьте нам ваши данные
                    и мы с вами свяжемся</p>
                <div class="input_descr">
                    <h3>Имя</h3>
                    <input type="text" placeholder="Евгений">
                </div>
                <div class="input_descr">
                    <h3>Телефон</h3>
                    <input type="text" placeholder="+7 555 656 20 25">
                </div>
                <div class="input_descr">
                    <h3>Email</h3>
                    <input type="text" placeholder="hardchehelrih@gmail.com|">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn_success">Отправить</button>
            </div>
        </div>
    </div>
</div>

@yield('scripts')
<script src="/public/media/client/js/app.js?v=0.1"></script>

</body>
</html>
