@extends('views.layouts.app')

@section('content')


    <section class="head_block">

        <div class="main_header_block">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12 col-sm-6 col-xl-6 col-lg-6">
                        <div class="main_descr">
                            <h3>{!!s_("Slider compani name","Главная","city puzzle")!!}</h3>
                            <h2>{!!s_("Slider title","Главная","city puzzle","textarea")!!}</h2>
                            <a class="sertificate_a" href="#">Купить сертификат</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 col-sm-6 col-xl-6 col-lg-6 main_content_wrap" style=" margin-top: auto; margin-bottom: auto; ">
                        <div class="main_content">
                            <h3>{!! s_("Slider right text","Главная","") !!}</h3>
                            <p>{!!s_("Slider right text 2","Главная","") !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#down-scroll" class="linkToBottom">{!!s_("button down","Главная","листай вниз","text")!!}</a>
    </section>

    <main id="down-scroll">
        <section class="about">
            <div class="container-fluid">
                <div class="row about_line">
                    <a href="#" class="col-md-6 col-12 col-sm-6 col-xl-6 col-lg-6 video_block">
                        <div>{!!s_("Video button","Главная","Смотреть видео","text")!!}</div>
                    </a>
                    <div class="col-md-6 col-12 col-sm-6 col-xl-6 col-lg-6 about_descr_wrap">
                        <div class="about_descr">
                            @if(s_("Video title","Главная","О компании","textarea")!="")
                                <h3>{!!s_("Video title","Главная","О компании","textarea")!!}</h3>
                            @endif
                            <p>{!!s_("Video content","Главная","","textarea")!!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="best_games" class="best_games">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12 col-sm-12 col-xl-12 col-lg-12">
                        <div class="best_games_title">
                            <h3>{!!s_("Recommendation before","Главная","это вам точно понравится","text")!!}</h3>
                            <h2>{!!s_("Recommendation title","Главная","наши лучшие игры","textarea")!!}</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-12 col-sm-12 col-xl-12 col-lg-12">
                        <div class="best_games_items">

                            @foreach(\App\Product::get() as $keyxs=>$game)

                                <div class="games" style="background-image: url('{{$game->images}}');">
                                    <div class="games-header">
                                        <h3>{!!s_("Game №","Главная","Игра номер","text")!!} {{$keyxs+1}}</h3>
                                        <span>{{LC($game->price)}}$</span>
                                    </div>
                                    <div class="games-footer">
                                        <h3>{!! LC($game->title) !!}</h3>
                                        <a href="{{url_custom('/game/'.$game->path)}}">{!!s_("button reservation","Главная","Забронировать","text")!!}</a>
                                    </div>
                                    <a  href="{{url_custom('/game/'.$game->path)}}" class="games-text" style=" width: 100%;text-decoration:none; ">
                                        <p>{!! LC($game->content_mini) !!}</p>
                                    </a>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="activity">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7 col-12 col-sm-7 col-xl-7 col-lg-7 activity_wrap">
                        <div class="activity_descr">
                            <h3>{!!s_("Event after","Главная","здесь вы узнаете","text")!!}</h3>
                            <h2>{!!s_("Event title","Главная","наши мероприятия","textarea")!!}</h2>
                            <p>{!!s_("Event content","Главная","","textarea")!!}</p>
                            <a href="{{url_custom('/activity')}}">{!!s_("Event button","Главная","Смотреть все мероприятия","textarea")!!}</a>
                        </div>
                    </div>
                    <div class="col-md-5 col-12 col-sm-5 col-xl-5 col-lg-5 activity_img"></div>
                </div>
            </div>
        </section>
    </main>


    <style>
        .head_block {
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>

@endsection
