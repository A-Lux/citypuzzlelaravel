@extends('views.layouts.app')

@section('content')

    <div class="activity_head" style=" display: flex; align-items: center; justify-content: center; ">
        <div class="main_header_block">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-12 col-sm-6 col-xl-7 col-lg-7">
                        <div class="main_descr_activity">
                            <h3>{!!s_("ACTIVITY before","МЕРОПРИЯТИЯ","The game","text")!!}</h3>
                            <h2>{!!s_("ACTIVITY title","МЕРОПРИЯТИЯ","The game","textarea")!!}</h2>
                            {!!s_("ACTIVITY content","МЕРОПРИЯТИЯ","","textarea")!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="activity_team">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 team_wrap">
                    <div class="team_descr">
                        <h3>{!!s_("ACTIVITY 2 before","МЕРОПРИЯТИЯ","мероприятия","text")!!}</h3>
                        <h2>{!!s_("ACTIVITY 2 title","МЕРОПРИЯТИЯ","корпоративы","textarea")!!}</h2>
                        {!!s_("ACTIVITY 2 content","МЕРОПРИЯТИЯ","","textarea")!!}
                    </div>
                </div>
                <div class="col-md-4 activity_content_bg"></div>
            </div>
        </div>
    </section>
    <section class="activity_team_2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 activity_content_bg_2"></div>
                <div class="col-md-8 team_wrap">
                    <div class="team_descr">
                        <h3>{!!s_("ACTIVITY 3 before","МЕРОПРИЯТИЯ","мероприятия","text")!!}</h3>
                        <h2>{!!s_("ACTIVITY 3 title","МЕРОПРИЯТИЯ","дни рождения","textarea")!!}</h2>
                        {!!s_("ACTIVITY 3 content","МЕРОПРИЯТИЯ","","textarea")!!}
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
