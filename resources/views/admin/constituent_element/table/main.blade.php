@php
    $columns=[];
        if($model_name!="table"){
           $model = app("\App\\" . $model_name);
           $columns = Schema::getColumnListing($model->getTable());
           }
@endphp
<table
    class="table sortTables @if(in_array("sort",$columns)) sortable  @endif   table-bordered table-striped js-table dataTable order-table no-footer"
    id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
    <thead>
    <tr role="row">

        @foreach($thead as $head)
            <th class="sorting {{$head=="id"?"fitwidth fitwidth-mini":""}}" tabindex="0"
                aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="">
                {{$head}}
            </th>
        @endforeach

        @if($model_name=="Product_price")
            <td>
                Даты
            </td>
        @endif
        <td class="fitwidth">
            Действие
        </td>
    </tr>
    </thead>
    <tbody class="ui-sortable">


    @foreach($tbody as $body)
        <tr role="row" class="even clickNexts ui-sortable-handle">
            @foreach($thead as $keyxs=>$head)
                <td class=" sorting_1 {{$keyxs=="id"?'fitwidth fitwidth-mini index':""}}" {{$keyxs=="id"?" data-id=".$body->id."":""}}>
                    @php
                        $return_data=($body->{$keyxs});
                    @endphp

                    @if($keyxs=="Product_id")
                        @php
                            $product=\App\Product::find($body->{$keyxs});
                                if(!is_null($product)){
                                $return_data=$product->title;
                                }
                        @endphp
                    @endif

                    @if(isset($exegesis))
                        @if(isset($exegesis[$keyxs]))
                            @if(isset($exegesis[$keyxs][$body->{$keyxs}]))
                                @php
                                    $return_data=  $exegesis[$keyxs][$body->{$keyxs}];
                                @endphp
                            @endif
                        @endif
                    @endif


                    {!! LC($return_data,"ru") !!}

                </td>
            @endforeach

                @if($model_name=="Product_price")
                    <td>
                        @php
                            $time_day=implode(",",array_keys(\App\DatTime::where("product_pric_id",$body->id)->get()->groupby("date")->toarray()));
                        @endphp
                        {{$time_day}}
                    </td>
                @endif
            <td class="fitwidth">

                <a class="nextlink btn btn-sm btn-info" href="{{url_custom($table_link[0].$body->{$table_link[1]})}}">Изменить</a>
                {{--                <a href="/admin/catalog/remove/2">Удалить</a>--}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
