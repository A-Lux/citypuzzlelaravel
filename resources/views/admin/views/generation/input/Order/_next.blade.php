<div class="language_grup_hid language_grup_multi language_grup_ru active ">

    <div class="asd">
        <label for="basic-url" class="label_control" style=" margin-bottom: 0.5rem; "> <b class="text_bigs">Номер
                брони</b></label>
        <div class="deffomsa"
             style=" padding: 0.5rem 1rem; background-color: #ccc; color: #fff; font-size: 18px; ">{{$model->order_number}}</div>
    </div>
    <div class="asd">
        <label for="basic-url" class="label_control" style=" margin-bottom: 0.5rem; "> <b
                class="text_bigs">Дата</b></label>
        <div class="deffomsa"
             style=" padding: 0.5rem 1rem; background-color: #ccc; color: #fff; font-size: 18px; ">{{$model->date}}</div>
    </div>
    <div class="asd">
        <label for="basic-url" class="label_control" style=" margin-bottom: 0.5rem; "> <b
                class="text_bigs">Время</b></label>
        <div class="deffomsa"
             style=" padding: 0.5rem 1rem; background-color: #ccc; color: #fff; font-size: 18px; ">{{$model->time}}</div>
    </div>
</div>

<style>
    .asd + .asd {
        margin-top: 1rem;
    }

    .asd {
        font-weight: 600;
    }
</style>
@php
    $model_old=$model;

    $model_name="Product";

    $model = app("\App\\" . $model_name);
    $sql_name = $model->getTable();
    $colum_list = Schema::getColumnListing($sql_name);

    $tbody = $model;
    if (in_array('sort', $colum_list) == true) {
        $tbody = $tbody->orderby("sort");
    }
    $tbody = $tbody->get();
    $thead = [];

    if(!is_null($model_old)){
      $tbody= $tbody->where("id",$model_old->game_id) ;
    }


    $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

    foreach ($meta_colum as $rows) {
        $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
        $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
    }
    $table_link = ["/admin/model/" . $model_name . "/", "id",(!is_null($model_old)?'?orders_id='.$model_old->id:'')];

@endphp

@if(!is_null($model_old))
    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Игра</h1>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif



@php
    $model_old=$model_old;

    $model_name="Order_item";

    $model = app("\App\\" . $model_name);
    $sql_name = $model->getTable();
    $colum_list = Schema::getColumnListing($sql_name);

    $tbody = $model;
    if (in_array('sort', $colum_list) == true) {
        $tbody = $tbody->orderby("sort");
    }
    $tbody = $tbody->get();
    $thead = [];

    if(!is_null($model_old)){
      $tbody= $tbody->where("orders_id",$model_old->id) ;
    }


    $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

    foreach ($meta_colum as $rows) {
        $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
        $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
    }
    $table_link = ["/admin/model/" . $model_name . "/", "id",(!is_null($model_old)?'?orders_id='.$model_old->id:'')];

@endphp

@if(!is_null($model_old))
    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Команды</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group">
                        <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?orders_id='.$model_old->id:''))}}"
                           class="btn btn-sm btn-outline-secondary waves-effect  ">Добавление команду</a>
                    </div>
                </div>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif
