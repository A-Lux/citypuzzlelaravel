
<div class="btnlink">

    @if($id!="0")
    <form method="post" action="{{url_custom('/admin/orders/edit/'.$id.'/updateStatus')}}">
        @csrf
        <button type="submit" name="status" value="accepted" class="btn btn-sm btn-outline-secondary waves-effect">Принять
        </button>
    </form>


    <form method="post" action="{{url_custom('/admin/orders/edit/'.$id.'/updateStatus')}}" style="margin-left: 1rem;">
        @csrf
        <button type="submit" name="status" value="done" class="btn btn-sm btn-outline-secondary waves-effect">Исполнен
        </button>
    </form>


    <div class="btn-group" style="margin-left: 1rem;">
        <a class="btn btn-sm btn-outline-secondary waves-effect" role="button" href="#" id="statusCancelled"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            Отменить
        </a>
        <div class="dropdown-menu" aria-labelledby="statusCancelled"
             style="padding: 2rem 1rem;width: 300px;margin-top: 1.5rem;">
            <form method="post" action="{{url_custom('/admin/orders/edit/'.$id.'/updateStatus')}}">

                @csrf
                <button type="submit" name="status" value="cancelled" class="btn btn-danger waves-effect ">
                    подтвердить
                </button>
            </form>
        </div>
    </div>

    @else
        <input type="hidden" name="status_save" value="0">

    @endif

</div>
<style>
    .btnlink {
        display: flex;
        flex-wrap: wrap;
        margin: 1rem 0;
        align-items: center;
    }
</style>
