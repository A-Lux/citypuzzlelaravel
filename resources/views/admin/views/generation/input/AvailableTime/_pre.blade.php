@php

    $product_id  = isset($_REQUEST["product_price_id"])? $_REQUEST["product_price_id"]:'';


    $product_id  = is_null($model)? $product_id: $model->product_price_id;

    $day= \App\Product_price::find($product_id);
 $game= \App\Product::find($day->Product_id);
@endphp


<ol class="breadcrumb hidden-xs">
    <li class="active">
        <a href="{{url_custom('/admin')}}"><i class="voyager-boat"></i> Dashboard</a>
    </li>
    <li>
        <a href="{{url_custom('/admin/model/Product')}}"><i class="voyager-boat"></i> Все игры</a>
    </li>
    @if(!is_null($game))
        <li>
            <a href="{{url_custom('/admin/model/Product/'.$game->id)}}"><i class="voyager-boat"></i> {!! LC($game->title) !!}</a>
        </li>
    @endif
    @if(!is_null($game))
        <li>
            <a href="{{url_custom('/admin/model/Product_price/'.$game->id)}}"><i class="voyager-boat"></i> {!! $day->date !!}</a>
        </li>
    @endif
</ol>
