@php

    $product_id  = isset($_REQUEST["Product_id"])? $_REQUEST["Product_id"]:'';


    $product_id  = is_null($model)? $product_id: $model->Product_id;

    $time_day="";
    if(!is_null($model)){
     $time_day=implode(",",array_keys(\App\DatTime::where("product_pric_id",$model->id)->get()->groupby("date")->toarray()));
    }
@endphp

{{maskInput($errors,["name"=>"date_save","class"=>"multidate","type"=>"date","attr"=>"select","placeholder"=>"Date","value"=>$time_day,"lang"=>0],$model)}}


<input type="hidden" name="Product_id_save" value="{{$product_id}}">
{{--<input type="hidden" name="path" value="{{url_custom("/admin/model/Product/".$product_id)}}">--}}

@if(!is_null($model))
    @php
        $model_old=$model;

        $model_name="AvailableTime";

        $model = app("\App\\" . $model_name);
        $sql_name = $model->getTable();
        $colum_list = Schema::getColumnListing($sql_name);

        $tbody = $model;
        if (in_array('sort', $colum_list) == true) {
            $tbody = $tbody->orderby("sort");
        }
        $tbody = $tbody->get();
        $thead = [];

        if(!is_null($model_old)){
          $tbody= $tbody->where("product_price_id",$model_old->id) ;
        }


        $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

        foreach ($meta_colum as $rows) {
            $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
            $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
        }
        $table_link = ["/admin/model/" . $model_name . "/", "id",(!is_null($model_old)?'?product_price_id='.$model_old->id:'')];

    @endphp


    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Время</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group">
                        <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?product_price_id='.$model_old->id:''))}}"
                           class="btn btn-sm btn-outline-secondary waves-effect  ">Добавление записи</a>
                    </div>
                </div>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif




