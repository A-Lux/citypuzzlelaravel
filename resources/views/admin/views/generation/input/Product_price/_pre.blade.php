@php

    $product_id  = isset($_REQUEST["Product_id"])? $_REQUEST["Product_id"]:'';


    $product_id  = is_null($model)? $product_id: $model->Product_id;

 $game= \App\Product::find($product_id);
@endphp


<ol class="breadcrumb hidden-xs">
    <li class="active">
        <a href="{{url_custom('/admin')}}"><i class="voyager-boat"></i> Dashboard</a>
    </li>
    <li>
        <a href="{{url_custom('/admin/model/Product')}}"><i class="voyager-boat"></i> Все игры</a>
    </li>
    @if(!is_null($game))
        <li>
            <a href="{{url_custom('/admin/model/Product/'.$game->id)}}"><i class="voyager-boat"></i> {!! LC($game->title) !!}</a>
        </li>
    @endif
</ol>
