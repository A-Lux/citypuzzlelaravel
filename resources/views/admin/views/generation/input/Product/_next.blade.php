@php
    $model_old=$model;

    $model_name="Product_price";

    $model = app("\App\\" . $model_name);
    $sql_name = $model->getTable();
    $colum_list = Schema::getColumnListing($sql_name);

    $tbody = $model;
    if (in_array('sort', $colum_list) == true) {
        $tbody = $tbody->orderby("sort");
    }
    $tbody = $tbody->get();
    $thead = [];

    if(!is_null($model_old)){
      $tbody= $tbody->where("Product_id",$model_old->id) ;
    }


    $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

    foreach ($meta_colum as $rows) {
        $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
        $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
    }
    $table_link = ["/admin/model/" . $model_name . "/", "id",(!is_null($model_old)?'?skill_id='.$model_old->id:'')];

@endphp

@if(!is_null($model_old))
    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Доступное время</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    {{--                    <div class="btn-group" style="margin-right: 1rem;">--}}
                    {{--                        <div class="row-fluid" >--}}
                    {{--                            <select class="selectpicker"  data-show-subtext="true"--}}
                    {{--                                    data-live-search="true">--}}
                    {{--                                <option value="">Выберите товар</option>--}}
                    {{--                                @foreach(\App\Product::where("id","!=",$model_old->id)->get() as $product)--}}
                    {{--                                    <option value="{{$product->id}}"--}}
                    {{--                                            {{ !is_null($model)?($model->Product_id==$product->id?'selected':'' ): ''}} data-subtext="/{{$product->path}}">{{strip_tags(LC($product->title))}}--}}
                    {{--                                    </option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="btn-group" style="margin-right: 2rem;display: none;">
                        <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?Product_id='.$model_old->id:''))}}"
                           class="btn btn-sm btn-info waves-effect  " style="display: flex;align-items: center;">Поделится
                            расписанием</a>
                    </div>
                    @if(count($tbody)==0)
                        <div class="btn-group">
                            <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?Product_id='.$model_old->id:''))}}"
                               class="btn btn-sm btn-outline-success waves-effect  "
                               style="display: flex;align-items: center;">Добавление дату</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif
