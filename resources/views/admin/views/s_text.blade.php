@extends('views.layouts.app')

@section('content')


    <div class="header">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-4 border-bottom">
            <h1 class="h2">Текста</h1>
            <div class="btn-toolbar mb-2 mb-md-0">


                <div class="langbtn"
                     style="margin-right: 2rem;padding-right: 2rem;border-right: 1px solid #000;display: flex;align-items: center;">
                    <p>
                        Выберите язык:
                    </p>
                    <?php
                    foreach (\App\Language::get() as $keyxs => $langs) {
                    ?>
                    <a href="javascript:void(0)"
                       data-lang="<?= $langs->name_key ?>"
                       class="btn btn-sm lang_control  btn-outline-success <?= $keyxs == 0 ? 'active' : ''?> waves-effect"
                       style="margin-left: 1rem;"><?= $langs->name ?></a>
                    <?php
                    }
                    ?>
                </div>

                <div class="btn-group " style="margin-left: 4rem;">
                    <a href="javascript:void(0);" onclick="$('#form_submit').submit()" type="submit"
                       class="btn btn-sm  btn-success waves-effect  px-4">Сохранить</a>
                </div>
            </div>
        </div>
    </div>


    <form id="form_submit" method="post" action="{{url_custom('/admin/update_text')}}" class="bodyMain">
        <div class="bds" style="margin-bottom: 2rem;">
            @foreach($thead_nav as $keys=>$nav)
                <div class="btn-group">
                    <a href="{{url_custom("/admin/StaticText?page=".$keys)}}"
                       class="btn btn-sm btn-outline-info waves-effect {{$page==$keys?"active":""}}  ">{{$keys}}</a>
                </div>
            @endforeach
        </div>

        @csrf
        @foreach($tbody as $input)
            @if($input->type_input=="time")
                {{maskInput($errors,["name"=>"save[".$input->name_key."]","type"=>"time","attr"=>"select","placeholder"=>$input->name_key,"value"=>$input->content,"lang"=>$input->languages])}}
            @elseif($input->type_input=="date")
                {{maskInput($errors,["name"=>"save[".$input->name_key."]","type"=>"date","attr"=>"select","placeholder"=>$input->name_key,"value"=>$input->content,"lang"=>$input->languages])}}
            @elseif($input->type_input=="images")
                {{maskInput($errors,["name"=>"save[".$input->name_key."]","type"=>"file","attr"=>"select","placeholder"=>$input->name_key,"value"=>$input->content,"lang"=>$input->languages])}}
            @elseif($input->type_input=="textarea")
                {{maskInput($errors,["name"=>"save[".$input->name_key."]","type"=>"textarea","placeholder"=>$input->name_key,"value"=>$input->content,"lang"=>$input->languages])}}
            @else
                {{maskInput($errors,["name"=>"save[".$input->name_key."]","type"=>"text","attr"=>"select","placeholder"=>$input->name_key,"value"=>$input->content,"lang"=>$input->languages])}}
            @endif
        @endforeach

        @if($page=="")
            <div class="s" style=" background-color: #fff; padding: 1.5rem; color: red; ">
                Выберите страницу
            </div>
        @endif


    </form>



    <div class="product" >
        <img class="product_img product_img-m" src="" alt="">
    </div>


@endsection
