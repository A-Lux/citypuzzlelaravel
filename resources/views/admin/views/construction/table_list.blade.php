@extends('views.layouts.app')

@section('content')


    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-4 border-bottom">
        <h1 class="h2">Таблицы</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group">
                <a href="{{url_custom("/admin/model/".$model_name."/0")}}"
                   class="btn btn-sm btn-outline-secondary waves-effect  ">Добавление записи</a>
            </div>
        </div>
    </div>

    <div class="bodyMain">

        @include('constituent_element.table.main')

    </div>

    <style>
        .table-bordered th, .table-bordered td {
            border: none;
        }

        .table-bordered {
            border: none;
        }

        .table-bordered th, .table-bordered td {
            border: none;
        }

        .table thead th {
            border: none;
        }

        .table tbody td {
            border-top: 1px solid #eaeaea;
        }
    </style>
@endsection
