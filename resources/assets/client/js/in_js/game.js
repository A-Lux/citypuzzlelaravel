'use strict';
import 'bootstrap';
import './team'
import {jsend, getFormData} from '../../../mics/js/app';
//Public Globals
const days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

let c_date = new Date();
let day = c_date.getDay();
let month = c_date.getMonth();
let year = c_date.getFullYear();

if ($("*").is("#app")) {
    (function App() {


        const calendar = `<div class="container">
            <div class="row">
                <div class="col-sm-12 col-12 calendar_wrap d-flex">
                    <div class="card flex-fill">
                        <div class="card-header d-flex justify-content-between">
                            <span class="prevMonth">&#8249;</span>
                            <span><strong id="s_m"></strong></span>
                            <span class="nextMonth">&#8250;</span>
                        </div>
                        <div class="card-body px-1 py-3 lazy-bk">
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless">
                                    <thead class="days text-center">
                                        <tr>
                                            ${Object.keys(days).map(key => (
            `<th><span>${days[key].substring(0, 3)}</span></th>`
        )).join('')}
                                        </tr>
                                    </thead>
                                    <tbody id="dates" class="dates text-center"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-12 d-flex pa-sm">
                    <div class="card border-0 mt-5 flex-fill d-none" id="event">
                        <div class="card-header py-3 text-center">
                            Add Event
                            <button type="button" class="close hide">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="card-body px-1 py-3">
                            <div class="text-center">
                                <span class="event-date">06 June 2020</span><br>
                                <span class="event-day">Monday</span>
                            </div>
                            <div class="events-today my-3 px-3">

                            </div>
                            <div class="input-group events-input mb-3 col-10 mx-auto mt-2">
                                <input type="text" class="form-control" placeholder="Add Event" id="eventTxt">
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" id="createEvent">+</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
        document.getElementById('app').innerHTML = calendar;
    })()


    var getRandomInt = function (max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    function renderCalendar(m, y) {
        //Month's first weekday
        let firstDay = new Date(y, m, 0).getDay();
        //Days in Month
        let d_m = new Date(y, m + 1, 0).getDate();
        //Days in Previous Month
        let d_pm = new Date(y, m, 0).getDate();


        let table = document.getElementById('dates');
        table.innerHTML = '';
        let s_m = document.getElementById('s_m');
        s_m.innerHTML = months[m] + ' ' + y;
        let date = 1;
        //remaing dates of last month
        let r_pm = (d_pm - firstDay) + 1;
        for (let i = 0; i < 6; i++) {
            let row = document.createElement('tr');
            for (let j = 0; j < 7; j++) {
                if (i === 0 && j < firstDay) {
                    let cell = document.createElement('td');
                    let span = document.createElement('span');
                    let cellText = document.createTextNode(r_pm);
                    span.classList.add('ntMonth');
                    span.classList.add('prevMonth');
                    cell.appendChild(span).appendChild(cellText);
                    row.appendChild(cell);
                    r_pm++;
                } else if (date > d_m && j < 7) {
                    if (j !== 0) {
                        let i = 0;
                        for (let k = j; k < 7; k++) {
                            i++
                            let cell = document.createElement('td');
                            let span = document.createElement('span');
                            let cellText = document.createTextNode(i);
                            span.classList.add('ntMonth');
                            span.classList.add('nextMonth');
                            cell.appendChild(span).appendChild(cellText);
                            row.appendChild(cell);
                        }
                        ;
                    }
                    break;
                } else {
                    let cell = document.createElement('td');
                    let span = document.createElement('span');
                    let cellText = document.createTextNode(date);
                    span.classList.add('showEvent');
                    span.dataset.date = ('20' + ('0' + y).slice(-2)) + '-' + ('0' + (m + 1)).slice(-2) + '-' + ('0' + (date)).slice(-2);
                    // if (date === c_date.getDate() && y === c_date.getFullYear() && m === c_date.getMonth()) {
                    //     span.classList.add('bg_border');
                    // }
                    cell.appendChild(span).appendChild(cellText);
                    row.appendChild(cell);
                    date++;
                }
            }
            table.appendChild(row);
        }

        var current_map = $(".showEvent").map(function (index, el) {
            return el.dataset.date;
        });
        delete current_map["prevObject"];
        delete current_map["length"];
        delete current_map["__proto__"];
        document.querySelector(' .card-body.lazy-bk').classList.remove("active");
        window.current_code = getRandomInt(10);
        jsend(document.querySelector("#app").dataset.url + "calendar_data", {
            "current": JSON.stringify(current_map),
            "id": $("#app").data("id")
        }, "window.calendar(data," + window.current_code + ");", "");

    }

    window.calendar = function (data, current_code) {

        if (window.current_code == current_code) {

            data.forEach(function (event) {

                var
                    ___this = document.querySelector(".showEvent[data-date='" + event["date"] + "']");
                if (___this != null) {
                    $(___this).data("day", event["day_id"]);
                    if (event["disable"] == "0") {
                        ___this.classList.add("bg_border");
                    } else {
                        ___this.classList.add("days_close");
                    }
                }

            });
            document.querySelector('.card-body.lazy-bk').classList.add("active");
        }
    }

    renderCalendar(month, year)


    $(function () {
        //     function showEvent(eventDate){
        //         let storedEvents = JSON.parse(localStorage.getItem('events'));
        //         if (storedEvents == null){
        //             $('.events-today').html('<h5 class="text-center">No events found</h5 class="text-center">');
        //         }else{
        //             let eventsToday = storedEvents.filter(eventsToday => eventsToday.eventDate === eventDate);
        //             let eventsList = Object.keys(eventsToday).map(k => eventsToday[k]);
        //             if(eventsList.length>0){
        //                 let eventsLi ='';
        //                 eventsList.forEach(event =>  $('.events-today').html(eventsLi +=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
        //                 ${event.eventText}
        //                 <button type="button" class="close remove-event" data-event-id="${event.id}" data-dismiss="alert" aria-label="Close">
        //                   <span aria-hidden="true">&times;</span>
        //                 </button>
        //               </div>`));
        //             }else{
        //                 $('.events-today').html('<h5 class="text-center">No events found</h5 class="text-center">');
        //             }
        //         }
        //     }
        //     function removeEvent(id){
        //         let storedEvents = JSON.parse(localStorage.getItem('events'));
        //         if(storedEvents != null){
        //             storedEvents = storedEvents.filter( ev => ev.id != id );
        //             localStorage.setItem('events', JSON.stringify(storedEvents));
        //             $('.toast-body').html('Your event have been removed');
        //             $('.toast').toast('show');
        //         }
        //     }
        //     $(document).on('click', '.remove-event', function(){
        //         let eventId = $(this).data('event-id');
        //         removeEvent(eventId);
        //     })

        $(document).on('click', '.prevMonth', function () {
            year = (month === 0) ? year - 1 : year;
            month = (month === 0) ? 11 : month - 1;
            renderCalendar(month, year);
        })
        $(document).on('click', '.nextMonth', function () {
            year = (month === 11) ? year + 1 : year;
            month = (month + 1) % 12;
            renderCalendar(month, year);
        })

        let days = document.querySelectorAll('.showEvent')
        days.forEach(item => {
            // if (item.textContent == 16) {
            //     item.classList.add('days_close')
            // }
        });

        $(document).on('click', '.showEvent.bg_border', function (e) {
            $('.showEvent').removeClass('active_calendar');
            $(this).addClass('active_calendar')
            $('#timeModal').modal('show');

            let dateTime = {
                day: e.target.textContent,
                month: s_m.textContent
            }

            localStorage.setItem('date', JSON.stringify(dateTime))
            let date = JSON.parse(localStorage.getItem('date'))

            jsend($("#app").data("url"), {"date": this.dataset.date, "id": $("#app").data("id")}, "window.sas(data);");
        })
        window.sas = function (data) {
            $(".time_a_wrap").html(data);
        }

        // $(document).on('click', '.showEvent', function(){
        //     $('.showEvent').removeClass('active_calendar');
        //     $('#event').removeClass('d-none');
        //     $(this).addClass('active_calendar');
        //     let todaysDate = $(this).text() +' '+ (months[month]) +' '+ year;
        //     let eventDay = days[new Date(year, month, $(this).text()).getDay()];
        //     let eventDate = $(this).text() + month + year;
        //     $('.event-date').html(todaysDate).data('eventdate', eventDate);
        //     $('.event-day').html(eventDay);
        //     showEvent(eventDate);
        // })
        //     $(document).on('click', '.hide', function(){
        //         $('#event').addClass('d-none');
        //     })
        //     $(document).on('click', '#createEvent', function(){
        //         let events = localStorage.getItem('events');
        //         let obj = [];
        //         if (events) {
        //             obj = JSON.parse(events);
        //         }
        //         let eventDate = $('.event-date').data('eventdate');
        //         let eventText = $('#eventTxt').val();
        //         let valid = false;
        //         $('#eventTxt').removeClass('data-invalid');
        //         $('.error').remove();
        //         if (eventText == ''){
        //             $('.events-input').append(`<span class="error">Please enter event</span>`);
        //             $('#eventTxt').addClass('data-invalid');
        //             $('#eventTxt').trigger('focus');
        //         }else if(eventText.length < 3){
        //             $('#eventTxt').addClass('data-invalid');
        //             $('#eventTxt').trigger('focus');
        //             $('.events-input').append(`<span class="error">please enter at least three characters</span>`);
        //         }else{
        //             valid = true;
        //         }
        //         if (valid){
        //             let id =1;
        //             if (obj.length > 0) {
        //                 id = Math.max.apply('', obj.map(function (entry) { return parseFloat(entry.id); })) + 1;
        //             }
        //             else {
        //                 id = 1;
        //             }
        //             obj.push({
        //                 'id' : id,
        //                 'eventDate': eventDate,
        //                 'eventText': eventText
        //             });
        //             localStorage.setItem('events', JSON.stringify(obj));
        //             $('#eventTxt').val('');
        //             $('.toast-body').html('Your event have been added');
        //             $('.toast').toast('show');
        //             showEvent(eventDate);
        //         }
        //     })
    })

    let video_block_game = document.querySelector('.game_video');
    let time_edit = document.querySelector('.time_edit')
    let reservation_btn = document.querySelector('.reservation_btn')

    time_edit.addEventListener('click', () => {

        $('#timeModal').modal('show')

    })

    if (video_block_game != null) {
        video_block_game.addEventListener('click', () => {
            $('#videoModal_game').modal('show')
        })
    }


    reservation_btn.addEventListener('click', () => {
        if ($(reservation_btn).hasClass("active")) {
            if ($(".result_time").html() != "") {
                $('#reservationModal').modal('show');
                var Info = window.teamInfo();
                $("#teamCount").html(Info["team"]);
                $(".team_people_all").html(parseInt(Info["team_people"]));
                $("#team_people_all").html(parseInt($(".prucebox").html()) * parseInt(Info["team_people"]));
            }
        }
    })


    let app_info = document.querySelectorAll('.app_info')
    let time_click = document.querySelector('.time_click');
    if (time_click != null) {
        time_click.addEventListener('click', () => {
            app_info[0].style.display = 'none'
            app_info[1].style.display = 'flex'
            $('#timeModal .close').click();
        });
    }

// let time_a = document.querySelectorAll('.time_a')
//
// time_a.forEach(item => {
//     item.addEventListener('click', e => {
//         time_a.forEach(i => {
//             if (i !== item) {
//
//                 i.classList.remove('time_active')
//
//             }
//         })
//     })
// })

    var monthRus = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Ноябрь',
        'Декабрь',
    ];
    var monthEng = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

    window.tems = [];


    window.teamRander = function () {
        var htmlpr = '';
        var teamCount = 0;
        window.tems.forEach(function (val, index) {
            htmlpr += '<li class="team_item">Tm#' + index + ': ' + val["team_name"] + ' <span><img data-id="' + index + '" class="edit" src="/public/media/client/img/edit_icon.png" alt=""><img class="delete" data-id="' + index + '" src="/public/media/client/img/delete.png" alt=""><img class="white_delete" data-id="' + index + '" src="/public/media/client/img/white_delete.png"></span></li>';
            teamCount++;
        });
        $(".list").html(htmlpr);

        if (teamCount > 0) {
            $(".reservation_btn").addClass("team_complit");

            if ($(".reservation_btn").hasClass("time_complit")) {
                $(".reservation_btn").addClass("active");
            } else {
                $(".reservation_btn").removeClass("active");
            }
        } else {
            $(".reservation_btn").removeClass("team_complit");
            $(".reservation_btn").removeClass("active");
        }
    }


    window.editTeam = function (team) {

        Object.keys(team).forEach(function (val, index) {
            if (val == "team_people") {
                $("select[name=team_people] option").attr('selected', 'false').prop('selected', 'false');
                $("select[name=team_people] option[value=" + team[val] + "]").attr('selected', 'true').prop('selected', 'true');
            } else {
                $("input[name=" + val + "]").val(team[val]);
            }
        });

    }

    window.deleteArr = function (arr, arrIndex) {
        return arr.slice(0, arrIndex).concat(arr.slice(arrIndex + 1));
    }


    $(document).on("click", ".delete", function () {
        window.tems = deleteArr(window.tems, $(this).data("id"));
        window.tems.sort();
        window.teamRander();
    });
    $(document).on("click", ".edit", function () {
        window.editTeam(window.tems[$(this).data("id")]);
        window.tems = deleteArr(window.tems, $(this).data("id"));
        window.teamRander();
    });
    $(document).on("submit", ".team_form", function () {
        var formData = getFormData($(this));
        tems.push(formData);
        $("input", this).val('');
        window.teamRander();
        return false;
    });

    window.teamInfo = function () {

        var all_team_people = 0;
        var tesnarra = window.tems;
        tesnarra.forEach(function (val, keu) {
            all_team_people += parseInt(val["team_people"]);
        });
        return {"team": (tesnarra.length), "team_people": all_team_people};
    }

    $(document).on("submit", "#reservationModal", function () {

        var formdate = getFormData($(this));
        var teanInfo = window.tems;

        formdate["team_info"] = JSON.stringify(teanInfo);
        jsend("/ru/almaty/order/new", formdate, 'window.okinfo(data);')
        $("#reservationModal").modal("hide");
        window.tems = [];
        window.teamRander();
        return false;
    });

    window.okinfo = function (data) {

        // $("#payment-form").modal("show");
        var stripe = Stripe($(".STRIPE_KEY").html());

        stripe.redirectToCheckout({
            sessionId: data["session_id"]
        }).then(function (result) {

        });

    }

    $(document).on("submit", "#submitday", function () {

        var formdate = getFormData($(this));
        $('#timeModal .close').click();
        $("#return_time").val(JSON.stringify(formdate));

        // let day = c_date.getDay();
        // let month = c_date.getMonth();
        // let year = c_date.getFullYear();

        $("#gane_date").val(formdate["date"]);
        $("#gane_time").val(formdate["time"]);

        var date = new Date(formdate["date"] + ' ' + formdate["time"]);

        $(".result_day").html(date.getDay() + ' ' + monthEng[date.getMonth()] + ' ' + date.getFullYear());

        date.setMilliseconds(2 * 60 * 60 * 1000);
        $(".result_time").html(formdate["time"] + " по " + date.getHours() + ':' + date.getMinutes());
        $(".result_time_before").html(formdate["time"] + " and before " + date.getHours() + ':' + date.getMinutes());
        let app_info = document.querySelectorAll('.app_info')
        app_info[0].style.display = 'none'
        app_info[1].style.display = 'flex'

        $(".reservation_btn").addClass("time_complit");

        if ($(".reservation_btn").hasClass("team_complit")) {
            $(".reservation_btn").addClass("active");
        } else {
            $(".reservation_btn").removeClass("active");
        }


        $("html, body").animate({scrollTop: ($("#team_descr").offset().top)}, 800);


        return false;
    });

    $(document).on("click", ".time_a", function () {

        $(this).addClass('time_active')

    });


    if (localStorage.getItem('time') == "undefined") {
        localStorage.setItem('time', JSON.stringify([]));
    }
    if (localStorage.getItem('time') == "undefined") {
        localStorage.setItem('date', JSON.stringify([]));
    }
    let time_input = JSON.parse(localStorage.getItem('time'))
    let date = JSON.parse(localStorage.getItem('date'))

    let invisible_input_time = document.querySelector('.invisible_input_time')
    let invisible_input_day = document.querySelector('.invisible_input_day')
    let invisible_input_year = document.querySelector('.invisible_input_year')

    console.log(invisible_input_time);

    invisible_input_time.value = time_input
    invisible_input_day.value = date.day
    invisible_input_year.value = date.month

    let contact_us = document.querySelectorAll('.contact_us')
    contact_us.forEach(item => {
        item.addEventListener('click', () => {
            $('#tocontactUsModal').modal('show')
        })
    });


// let sertificate_a = document.querySelector('.sertificate_a')
// if (sertificate_a) {
//     sertificate_a.addEventListener('click', () => {
//         $('#sertificateModal').modal('show')
//     })
// }
//
// sertificate_a.addEventListener('click', () => {
//     $('#sertificateModal').modal('show')
// })
}
