import 'bootstrap';

let sertificate_a = document.querySelector('.sertificate_a')
if (sertificate_a) {
  sertificate_a.addEventListener('click', () => {
    $('#sertificateModal').modal('show')
  })
}
let video_block = document.querySelector('.video_block')
if (video_block) {
  video_block.addEventListener('click', () => {
    $('#videoModal').modal('show')
  })
}
