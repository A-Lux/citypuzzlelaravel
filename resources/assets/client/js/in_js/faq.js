var acc = document.querySelectorAll(".accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active_faq");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

let contact_us = document.querySelectorAll('.contact_us')

contact_us.forEach(item => {
  item.addEventListener('click', () => {
    $('.toogle-close').click()
    $('#tocontactUsModal').modal('show')
  })
})