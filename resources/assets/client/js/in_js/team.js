// let team_btn = document.querySelector('.team_btn')
// let team_name = document.querySelector('.team_name')
// let team_people = document.querySelector('.team_people')
// let last_name = document.querySelector('.last_name')
// let first_name = document.querySelector('.first_name')
// let mobile = document.querySelector('.mobile_number')
// let email = document.querySelector('.email')
// let people = 2
// let list = document.querySelector('.list')
// let save_edit = document.querySelector('.save_edit')
// let reservation_btn = document.querySelector('.reservation_btn')
// let game_p = document.querySelector('.game_p')
// let team_edit_name = document.querySelector('.team_edit_name')
// let game_select_h2 = document.querySelector('.game_select_h2')
//
// if (JSON.parse(localStorage.getItem('teamInfo'))) {
//   var team_info = JSON.parse(localStorage.getItem('teamInfo'))
// } else {
//   var team_info = []
// }
//
// // team_people.addEventListener('change', e => {
// //   people = e.target.value
// // })
//
// team_info.forEach((item, index) => {
//   const li = document.createElement('li')
//   li.classList.add('team_item')
//   list.appendChild(li)
//   li.innerHTML = `Tm#${index + 1} ${item.team_name}<span><img class="edit" src="img/edit_icon.png" alt=""><img class="delete" src="img/delete.png" alt=""><img class="white_delete" src="img/white_delete.png"></span></li>`
//   let del = li.querySelector('.delete')
//   let white_del = li.querySelector('.white_delete')
//   let edit = li.querySelector('.edit')
//   del.addEventListener('click', () => {
//     li.remove()
//     if ( index == li.textContent[3] - 1 ) {
//       team_info.splice(index, 1)
//       let serialForm = JSON.stringify(team_info)
//       localStorage.setItem('teamInfo', serialForm)
//     }
//   })
//   white_del.addEventListener('click', () => {
//     li.remove()
//     if ( index == li.textContent[3] - 1 ) {
//       team_info.splice(index, 1)
//       let serialForm = JSON.stringify(team_info)
//       localStorage.setItem('teamInfo', serialForm)
//     }
//   })
//   edit.addEventListener('click', () => {
//     li.classList.add('time_active')
//     white_del.style.display = 'inline-flex'
//     del.style.display = 'none'
//     edit.style.display = 'none'
//     save_edit.style.display = 'block'
//     reservation_btn.style.display = 'none'
//     team_btn.style.display = 'none'
//     game_p.style.display = 'none'
//     team_edit_name.style.display = 'block'
//
//     game_select_h2.innerText = `Edit team #${li.textContent[3]}`
//     team_edit_name.innerText = li.textContent
//
//     let editId = JSON.stringify(item.id)
//     localStorage.setItem('id', editId)
//
//     team_name.value = item.team_name,
//     last_name.value = item.last_name,
//     first_name.value = item.first_name,
//     mobile.value = item.mobile,
//     email.value = item.email,
//     team_people.value = item.people
//
//     let editAll = document.querySelectorAll('.team_item')
//     editAll.forEach(item => {
//       if (li !== item) {
//         item.classList.remove('time_active')
//         white_del.style.display = 'none'
//         del.style.display = 'inline-flex'
//         edit.style.display = 'inline-flex'
//       }
//     })
//   })
//   save_edit.addEventListener('click', (e) => {
//     li.classList.remove('time_active')
//     white_del.style.display = 'none'
//     del.style.display = 'inline-flex'
//     edit.style.display = 'inline-flex'
//     save_edit.style.display = 'none'
//     reservation_btn.style.display = 'block'
//     team_btn.style.display = 'block'
//     game_p.style.display = 'block'
//     team_edit_name.style.display = 'none'
//
//     for(let i = 0; i < team_info.length; i++) {
//       let id = JSON.parse(localStorage.getItem('id'))
//       if(team_info[i].id === id) {
//         team_info[i].team_name = team_name.value
//         team_info[i].last_name = last_name.value
//         team_info[i].first_name = first_name.value
//         team_info[i].mobile = mobile.value
//         team_info[i].email = email.value
//         team_info[i].team_people = team_people.value
//       }
//     }
//     let serialForm = JSON.stringify(team_info)
//     localStorage.setItem('teamInfo', serialForm)
//     window.location.reload()
//   })
// })
//
// function create_team() {
//   if (team_info.length == 0) {
//     var id = 0;
//   } else {
//     var id = team_info.length;
//   }
//   team_info.push(
//     {
//       id: id,
//       team_name: team_name.value,
//       last_name: last_name.value,
//       first_name: first_name.value,
//       mobile: mobile.value,
//       email: email.value,
//       people: people
//     })
//     let serialForm = JSON.stringify(team_info)
//     localStorage.setItem('teamInfo', serialForm)
//     const li = document.createElement('li')
//     li.classList.add('team_item')
//     list.appendChild(li)
//     li.innerHTML = `Tm#${team_info.length} ${team_name.value}<span><img class="edit" src="img/edit_icon.png" alt=""><img class="delete" src="img/delete.png" alt=""><img class="white_delete" src="img/white_delete.png"></span></li>`
//     let del = li.querySelector('.delete')
//     let white_del = li.querySelector('.white_delete')
//     let edit = li.querySelector('.edit')
//     del.addEventListener('click', () => {
//       li.remove()
//       if ( team_info.length == li.textContent[3]) {
//         team_info.splice(li.textContent[3] - 1, 1)
//         let serialForm = JSON.stringify(team_info)
//         localStorage.setItem('teamInfo', serialForm)
//       }
//     })
//     white_del.addEventListener('click', () => {
//       li.remove()
//       if ( team_info.length == li.textContent[3]) {
//         team_info.splice(li.textContent[3] - 1, 1)
//         let serialForm = JSON.stringify(team_info)
//         localStorage.setItem('teamInfo', serialForm)
//       }
//     })
//     edit.addEventListener('click', () => {
//       li.classList.add('time_active')
//       del.style.display = 'none'
//       edit.style.display = 'none'
//       white_del.style.display = 'inline-flex'
//       save_edit.style.display = 'block'
//       reservation_btn.style.display = 'none'
//       team_btn.style.display = 'none'
//       game_p.style.display = 'none'
//       team_edit_name.style.display = 'block'
//
//       game_select_h2.innerText = `Edit team #${li.textContent[3]}`
//       team_edit_name.innerText = li.textContent
//       team_info.forEach((item, index) => {
//         if (index + 1 == li.textContent[3]) {
//           let editId = JSON.stringify(item.id)
//           localStorage.setItem('id', editId)
//
//           team_name.value = item.team_name,
//           last_name.value = item.last_name,
//           first_name.value = item.first_name,
//           mobile.value = item.mobile,
//           email.value = item.email,
//           team_people.value = item.people
//         }
//       })
//
//       let editAll = document.querySelectorAll('.team_item')
//       editAll.forEach(item => {
//         if (li !== item) {
//           item.classList.remove('time_active')
//           white_del.style.display = 'none'
//           del.style.display = 'inline-flex'
//           edit.style.display = 'inline-flex'
//         }
//       })
//     })
//     save_edit.addEventListener('click', () => {
//       li.classList.remove('time_active')
//       white_del.style.display = 'none'
//       del.style.display = 'inline-flex'
//       edit.style.display = 'inline-flex'
//       save_edit.style.display = 'none'
//       reservation_btn.style.display = 'block'
//       team_btn.style.display = 'block'
//       game_p.style.display = 'block'
//       team_edit_name.style.display = 'none'
//
//
//       for(let i = 0; i < team_info.length; i++) {
//         let id = JSON.parse(localStorage.getItem('id'))
//         if(team_info[i].id === id) {
//           team_info[i].team_name = team_name.value
//           team_info[i].last_name = last_name.value
//           team_info[i].first_name = first_name.value
//           team_info[i].mobile = mobile.value
//           team_info[i].email = email.value
//           team_info[i].team_people = team_people.value
//         }
//       }
//       let serialForm = JSON.stringify(team_info)
//       localStorage.setItem('teamInfo', serialForm)
//       window.location.reload()
//     })
// }
//
// function input_clear() {
//   team_name.value = '',
//   last_name.value = ''
//   first_name.value = ''
//   mobile.value = ''
//   email.value = ''
//   team_people.value = 2
// }
//
// team_btn.addEventListener('click', () => {
//   create_team()
//   input_clear()
//
// })
//
// let contact_us = document.querySelectorAll('.contact_us')
//
// contact_us.forEach(item => {
//   item.addEventListener('click', () => {
//     $('#tocontactUsModal').modal('show')
//   })
// })
