export function parInt(number) {
    return parseInt(String(number).replace(/[^-0-9]/gim, '').replace(/\s/g, ''));
}

export function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};


    $.map(unindexed_array, function (n, i) {



        if (typeof indexed_array[n['name']] != "undefined") {


            if (Array.isArray(indexed_array[n['name']])) {
                indexed_array[n['name']].push(n['value'])
            } else {
                indexed_array[n['name']] = [n['value'], indexed_array[n['name']]];
            }

        } else {
            indexed_array[n['name']] = n['value'];
        }


    });

    return indexed_array;
}

export function inputStr(number) {

    return (String(number).replace(/[^-0-9]/gim, '').replace(/\s/g, '')).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
}
export function jsend(valuehg, dataelx, evalx, errorj) {


    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'),
        dateof = {_token: CSRF_TOKEN},
        deteextend = $.extend(dataelx, dateof);


    if (typeof evalx == "undefined") {
        evalx = "";
    }

    if (typeof errorj == "undefined") {
        errorj = "";
    }


    $.ajax({
        /* the route pointing to the post function */
        url: valuehg,
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: deteextend,
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) {

            eval(evalx);

        },
        error: function (data) {
            eval(errorj);
        }
    });

}
